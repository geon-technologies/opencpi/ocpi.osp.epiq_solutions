.. data_src_qadc_csts_test_app application

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. Company:     Geon Technologies, LLC
   Author:      AP
   Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
                Dissemination of this information or reproduction of this
                material is strictly prohibited unless prior written
                permission is obtained from Geon Technologies, LLC

:orphan:

.. _data_src_qadc_csts_test_app:

app: ``data_src_qadc_csts_test_app``
====================================

Description
-----------

Tests the digital RX path from the AD9361 through the ``data_src_qadc_csts.hdl`` worker.
The single source is the built-in pattern generator of the AD9361.

.. warning::

   This does not test the RF Rx path of the AD9361

Hardware Portability
^^^^^^^^^^^^^^^^^^^

Currently intended only for platforms with an AD9361 (which has a DAC). Future testing is
expected to support more platform scenarios.

Prerequisites
-------------

The ``z3u`` has been properly installed and deployed, and the application assembly
bitstream is built. Specifically, this test application requires that the following
assets are built:

  * HDL Assembly: ``data_src_qadc_csts_test_asm`` (from the Z3u OSP)
  * RCC Worker: ``file_write.rcc`` (from ``core`` project)
  * RCC Proxy: ``platform_ad9361_config_proxy_csts.rcc`` (from ``platform`` project)

The test application is built/cross-compiled (on HOST):

  * ``$ cd opencpi/projects/osps/ocpi.osp.epiq_solutions/applications/data_src_qadc_csts_test_app``
  * ``$ ocpidev build --rcc-platform xilinx18_3_aarch64``

Execution
---------

The execution commands herein are subject to change. This application can not be run with ocpirun,
due to reliance on ACI-specific BER calculation. Note that <path_to_app_xml> can be any OAS from a
data_src_qadc_csts_test_app application directory from any project. OSP projects which support this
app will have a OSP-specific OAS which may be used with the assets executable.

.. note::

   The instructions are written for **Network Mode**

#. Boot the Matchstiq Z3U, setup for Network Mode, then browse to the following directory:

   ``% cd /mnt/ocpi_z3u/applications/data_src_qadc_csts_test_app/``

#. Setup the artifact search path:

   ``% export OCPI_LIBRARY_PATH=../../artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_platform/artifacts:/mnt/ocpi_assets/artifacts``

#. Run the application

   ``% ./target-xilinx18_3_aarch64/data_src_qadc_csts_test_app -d -t 3 data_src_qadc_csts_z3u_test_app.xml``

#. Verification

   Upon completion of successful test, the application returns an exit status of 0. The application
   will also print the following to the screen upon success, where the actual "number_of_*" may
   vary:

Example of stdout
^^^^^^^^^^^^^^^^^

.. code-block::

   ...
   ...
   ...
   filename : data_src_qadc_csts_test_app.out
   number_of_processed_bits_shifted_through_LFSR : 8203
   number_of_total_bits_in_file : 262144
   number_of_processed_bits_in_file : 196608
   estimated_number_of_bit_errors : 0
   estimated_BER : 0%
   SUCCESS: all data received without bit error
