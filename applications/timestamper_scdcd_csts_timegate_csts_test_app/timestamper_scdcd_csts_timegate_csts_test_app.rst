.. timestamper_scdcd_csts_timegate_csts_test_app application

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. Company:     Geon Technologies, LLC
   Author:      AP
   Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
                Dissemination of this information or reproduction of this
                material is strictly prohibited unless prior written
                permission is obtained from Geon Technologies, LLC

:orphan:

.. _timestamper_scdcd_csts_timegate_csts_test_app:

app: ``timestamper_scdcd_csts_timegate_csts_test_app``
======================================================

Description
-----------

.. warning::

   This test app was copied from another FOSS OSPs (E3xx, PlutoSDR) and modified to
   support the csts-protocol in support of the Z3u.

   THIS APP IS PROVIDED AS A STARTING POINT AND NOT MANDATORY FOR THE ACCEPTANCE
   OF THE Z3u OSP.

   THIS APP EXECUTES "SUCCESSFULLY" BUT REQUIRES ADDITIONAL TOOLING TO BE PROVIDED
   BY FOSS. IT REQUIRES THE GENERATION OF A TEST INPUT FILE WHICH INCLUDES THE
   CORRECT MESSAGING FOR TIMED TRANMIT AND TOOLING TO VALIDATE THE CAPTURED DATA.

Reads from a file which has messaging information that controls the timed-transmit
of a tone.

The DRC used by this implementation does include dynamic configuration of the
RF filter path.

Prerequisites
-------------

The ``z3u`` has been properly installed and deployed, and the application assembly
bitstream is built. Specifically, this test application requires that the following
assets are built:

  * HDL Assembly: ``timestamper_scdcd_csts_timegate_csts_asm`` (from the Z3u OSP)
  * DRC Worker: ``drc_z3u_mode_2_cmos_ts_timegate_csts.rcc`` (from the Z3u OSP)

Execution
---------

.. note::

   The instructions are written for **Network Mode**

#. Boot the Matchstiq Z3U, setup for Network Mode, then browse to the following directory:

   ``% cd /mnt/ocpi_z3u/applications/timestamper_scdcd_csts_timegate_csts_test_app/``

#. Setup the artifact search path:

   ``% export OCPI_LIBRARY_PATH=../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm:../../artifacts:/mnt/ocpi_platform/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts``

#. (OPTIONAL mode #x) Setup External Signal Generator

   Review comments within the run_test_app.sh to determine desired test mode.

#. Run the application

   #. Execute script with desire arguments: ./scripts/run_test_app.sh

      ``% ./scripts/run_test_app.sh``

      .. warning::

         The app does not always gracefully finish and sometimes requires the user to execute "Ctrl-C"
         for termination

      ..

#. Verify output:

   #. Post-processing of the captured data is not yet supported. However:

      #. It is expected that with a proper input file, the SpecA would show a transmitted
	 tone toggling on/off with a know rate.

      #. Captured data should show the expected time samples field increment in a
	 consistent, predetermined manner.

Example of stdout
^^^^^^^^^^^^^^^^^

.. code-block::

   % export OCPI_LIBRARY_PATH=../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm:../../artifacts:/mnt/ocpi_platform/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts
   % ./scripts/run_test_app.sh
   Setting the time_now property to '0' on instance 'p/time_server'
   Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
   Actual deployment is:
   Instance  0 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in /mnt/ocpi_core/artifacts/ocpi.core.file_read.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:08:09 2021
   Instance  1 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in /mnt/ocpi_core/artifacts/ocpi.core.file_write.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:08:36 2021
   Instance  2 drc (spec ocpi.platform.drc) on rcc container 1: rcc0, using drc_z3u_mode_2_cmos_ts_timegate_csts in ../../artifacts/ocpi.osp.epiq_solutions.devices.drc_z3u_mode_2_cmos_ts_timegate_csts.rcc.0.xilinx18_3_aarch64.so dated Thu
   Jan 13 11:02:02 2022
   Instance  3 drc.config (spec ocpi.platform.devices.platform_ad9361_config_csts) on hdl container 0: PL:0, using platform_ad9361_config_csts/c/platform_ad9361_config_csts in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/c
   ontainer-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  4 drc.data_sub (spec ocpi.platform.devices.platform_csts_ad9361_data_sub) on hdl container 0: PL:0, using platform_csts_ad9361_data_sub-3/c/platform_csts_ad9361_data_sub in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_
   csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  5 drc.rx_qadc0 (spec ocpi.platform.devices.data_src_qadc_csts) on hdl container 0: PL:0, using data_src_qadc_csts-1/c/data_src_qadc_csts0 in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_s
   cdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  6 drc.rx_csts_to_iqstream0 (spec ocpi.assets.misc_comps.csts_to_iqstream) on hdl container 0: PL:0, using timestamper_scdcd_csts/a/timestamper_scdcd_csts in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/contain
   er-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  7 drc.rx_complex_mixer0 (spec ocpi.assets.dsp_comps.complex_mixer) on hdl container 0: PL:0, using timegate_csts/a/timegate_csts in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts
   _timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  8 drc.rx_cic_dec0 (spec ocpi.assets.dsp_comps.cic_dec) on hdl container 0: PL:0, using data_sink_qdac_csts/c/data_sink_qdac_csts in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts
   _timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance  9 drc.tx_cic_int0 (spec ocpi.assets.dsp_comps.cic_int) on hdl container 0: PL:0, using pcal6524/c/pcal65240 in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_csts_asm_z3
   u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance 10 drc.tx_iqstream_to_csts0 (spec ocpi.assets.misc_comps.iqstream_to_csts) on hdl container 0: PL:0, using pcal6524/c/pcal65241 in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_t
   imegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance 11 drc.tx_qdac0 (spec ocpi.platform.devices.data_sink_qdac_csts) on hdl container 0: PL:0, using pcal6524/c/pcal65242 in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/container-timestamper_scdcd_csts_timegate_cs
   ts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Instance 12 drc.ad9361_spi (spec ocpi.platform.devices.platform_ad9361_spi_csts) on hdl container 0: PL:0, using platform_ad9361_spi_csts/c/platform_ad9361_spi_csts in ../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm/contai
   ner-timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/timestamper_scdcd_csts_timegate_csts_asm_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:14:13 2022
   Application XML parsed and deployments (containers and artifacts) chosen [14 s 405 ms]
   Application established: containers, workers, connections all created [0 s 170 ms]
   ad9361_init : AD936x Rev 2 successfully initialized
   DRC: RX tuning_freq_MHz: 2450.000000
   DRC: band winner!
   Configure filter path
   DRC: PCAL_2001 configured!
   Select RX2 port of AD9361 and RF path
   pcal:2001 port 2:0x8A
   pcal:2008 port 2:0x29
   pcal:2008 port 2:0x29
   pcal:2008 port 2:0x29
   pcal:2001 port 2:0x8A
   pcal:2001 port 2:0x8A
   pcal:2001 port 2:0x8A
   Select TRX1 port of AD9361 and RF path
   pcal:2008 port 1:0x82
   pcal:2008 port 2:0x29
   pcal:2008 port 2:0x29
   pcal:2008 port 2:0x29
   pcal:2008 port 2:0x29
   pcal:2008 port 2:0x29
   pcal:2008 port 1:0x82
   DRC: TX is less than 3000
   DRC: TX tuning_freq_MHz: 2450.000000
   Application started/running [1 s 35 ms]
   Waiting for up to 5 seconds for application to finish
   Application is now considered finished after waiting 5 seconds [5 s 9 ms]
   Instance p/time_server of io worker time_server (spec ocpi.core.devices.time_server) with index 1
   0           ocpi_debug: false
   1          ocpi_endian: little
   2         ocpi_version: 0x0
   3             time_now: 0x165c2e5cda
   4                delta: 0x0
   5               PPS_ok: false
   6 enable_time_now_updates_from_PPS: <unreadable>
   7 valid_requires_write_to_time_now: <unreadable>
   8            frequency: 100000000
   9    PPS_tolerance_PPM: 0x3e8
   10             PPS_test: false
   11 clr_status_sticky_bits: <unreadable>
   12 force_time_now_to_free_running: <unreadable>
   13       PPS_out_source: <unreadable>
   14 force_time_now_valid: <unreadable>
   15 force_time_now_invalid: <unreadable>
   16 PPS_lost_sticky_error: false
   17 time_now_updated_by_PPS_sticky: false
   18  time_now_set_sticky: true
   19 PPS_lost_last_second_error: false
   20            PPS_count: 0x2
   21     ticks_per_second: 0x0
   22            using_PPS: true
   23           time_valid: true
