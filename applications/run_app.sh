cd data_sink_qdac_csts_test_app
export OCPI_LIBRARY_PATH=../../artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_platform/artifacts:/mnt/ocpi_assets/artifacts
./target-xilinx18_3_aarch64/data_sink_qdac_csts_test_app -d -t 3 data_sink_qdac_csts_z3u_test_app.xml

cd ../data_src_qadc_csts_test_app
export OCPI_LIBRARY_PATH=../../artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_platform/artifacts:/mnt/ocpi_assets/artifacts
./target-xilinx18_3_aarch64/data_src_qadc_csts_test_app -d -t 3 data_src_qadc_csts_z3u_test_app.xml

cd ../FSK
export OCPI_LIBRARY_PATH=../../hdl/assemblies/fsk_filerw:/mnt/ocpi_assets/artifacts:/mnt/ocpi_core/artifacts
ocpirun -v -d app_fsk_filerw.xml

cd ../fsk_dig_radio_ctrlr_csts
export OCPI_LIBRARY_PATH=../../hdl/assemblies/fsk_modem_csts:../../hdl/devices:/mnt/ocpi_platform/artifacts:/mnt/ocpi_assets/artifacts:/mnt/ocpi_core/artifacts
ocpirun -v -t 15 fsk_modem_app.xml


#cd ../timegate_csts_test_app
#export OCPI_LIBRARY_PATH=../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm:../../hdl/devices/drc_z3u_mode_2_cmos_timegate_csts.rcc:/mnt/ocpi_platform/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts
#./scripts/run_test_app.sh

#cd ../timestamper_scdcd_csts_test_app
#export OCPI_LIBRARY_PATH=../../hdl/assemblies/timestamper_scdcd_csts_asm:../../hdl/devices/drc_z3u_mode_2_cmos_ts_timegate_csts.rcc:/mnt/ocpi_platform/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts
#./scripts/run_test_app.sh

#cd ../timestamper_scdcd_csts_timegate_csts_timegate_csts_test_app
#export OCPI_LIBRARY_PATH=../../hdl/assemblies/timestamper_scdcd_csts_timegate_csts_asm:../../hdl/devices/drc_z3u_mode_2_cmos_ts_timegate_csts.rcc:/mnt/ocpi_platform/artifacts:/mnt/ocpi_core/artifacts:/mnt/ocpi_assets/artifacts

export OCPI_LIBRARY_PATH=../../artifacts

cd ../z3u_i2c_bus0_test_app
ocpirun -v -d -t 1 z3u_i2c_bus0_test_app.xml
cd ../z3u_i2c_bus1_test_app
ocpirun -v -d -t 1 z3u_i2c_bus1_test_app.xml
cd ../z3u_i2c_bus2_test_app
ocpirun -v -d -t 1 z3u_i2c_bus2_test_app.xml
