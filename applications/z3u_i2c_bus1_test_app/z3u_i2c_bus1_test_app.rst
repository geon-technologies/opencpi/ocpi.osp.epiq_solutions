.. z3u_i2c_bus1_test_app application

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. Company:     Geon Technologies, LLC
   Author:      AP
   Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
                Dissemination of this information or reproduction of this
                material is strictly prohibited unless prior written
                permission is obtained from Geon Technologies, LLC

:orphan:

.. _z3u_i2c_bus1_test_app:

app: ``z3u_i2c_bus1_test_app``
==========================

Description
-----------

Tests the access of the various I2C devices of the "bus1" of the Z3u. Generally, reading of the
device worker properties is performed, but writing of some properties may also be performed.

This document assumes that the user has reviewed the datasheets for the RCC and HDL workers that
are present in this application (pcal6524).

**pcal6524**: These components (x3) in the application configured the RF pathways on the Z3u.

Hardware Portability
-------------------

The I2C devices workers implemented on this bus are not unique to the Z3u and it is possible
to reuse on other platforms. However, the subdevice worker use here is not likely portable,
therefore a new one for the target platform will need to be created.

Prerequisties
-------------

The ``z3u`` has been properly installed and deployed, and the application assembly
bitstream is built. Specifically, this test application requires that the following
assets are built:

  * HDL Assembly: ``empty`` with container ``cnt_z3u_i2c_bus1.xml`` (from the Z3u OSP)
  * RCC Proxy: ``pcal6524_proxy.rcc`` (from the Z3u OSP)

Execution
---------

.. note::

   The instructions are written for **Network Mode**

..

#. Boot the Matchstiq Z3U, setup for Network Mode, then browse to the following directory:

   ``% cd /mnt/ocpi_z3u/applications/z3u_i2c_bus1_test_app/``

#. Setup the artifact search path:

   ``% export OCPI_LIBRARY_PATH=../../artifacts``

#. Run the application

   ``% ocpirun -v -d -x -t 1 z3u_i2c_bus1_test_app.xml``

Example of stdout
~~~~~~~~~~~~~~~~~

.. code-block::

   Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
   Actual deployment is:
   Instance  0 pcal6524_proxy0 (spec ocpi.osp.epiq_solutions.devices.pcal6524_proxy) on rcc container 1: rcc0, using pcal6524_proxy in ../../artifacts/ocpi.osp.epiq_solutions.devices.pcal6524_proxy.rcc.0.xilinx18_3_aarch64.so dated Tue Dec  7 17:32:25 2021
   Instance  1 pcal6524_proxy1 (spec ocpi.osp.epiq_solutions.devices.pcal6524_proxy) on rcc container 1: rcc0, using pcal6524_proxy in ../../artifacts/ocpi.osp.epiq_solutions.devices.pcal6524_proxy.rcc.0.xilinx18_3_aarch64.so dated Tue Dec  7 17:32:25 2021
   Instance  2 pcal6524_proxy2 (spec ocpi.osp.epiq_solutions.devices.pcal6524_proxy) on rcc container 1: rcc0, using pcal6524_proxy in ../../artifacts/ocpi.osp.epiq_solutions.devices.pcal6524_proxy.rcc.0.xilinx18_3_aarch64.so dated Tue Dec  7 17:32:25 2021
   Instance  3 pcal6524_proxy0.pcal6524 (spec ocpi.osp.epiq_solutions.devices.pcal6524) on hdl container 0: PL:0, using pcal6524/c/pcal65240 in ../../artifacts/ocpi.osp.epiq_solutions.empty_z3u_base_cnt_z3u_i2c_bus1.hdl.0.z3u.bitz dated Fri Jan  7 20:19:43 2022
   Instance  4 pcal6524_proxy1.pcal6524 (spec ocpi.osp.epiq_solutions.devices.pcal6524) on hdl container 0: PL:0, using pcal6524/c/pcal65241 in ../../artifacts/ocpi.osp.epiq_solutions.empty_z3u_base_cnt_z3u_i2c_bus1.hdl.0.z3u.bitz dated Fri Jan  7 20:19:43 2022
   Instance  5 pcal6524_proxy2.pcal6524 (spec ocpi.osp.epiq_solutions.devices.pcal6524) on hdl container 0: PL:0, using pcal6524/c/pcal65242 in ../../artifacts/ocpi.osp.epiq_solutions.empty_z3u_base_cnt_z3u_i2c_bus1.hdl.0.z3u.bitz dated Fri Jan  7 20:19:43 2022
   Application XML parsed and deployments (containers and artifacts) chosen [10 s 692 ms]
   Application established: containers, workers, connections all created [0 s 31 ms]
   Dump of all initial property values:
   Property   9: pcal6524_proxy0.pcal6524.input_port_0 = "0x51"
   Property  10: pcal6524_proxy0.pcal6524.input_port_1 = "0x0"
   Property  11: pcal6524_proxy0.pcal6524.input_port_2 = "0x29"
   Property  12: pcal6524_proxy0.pcal6524.reserved00 = "0xc0"
   Property  13: pcal6524_proxy0.pcal6524.output_port_0 = "0x51"
   Property  14: pcal6524_proxy0.pcal6524.output_port_1 = "0x0"
   Property  15: pcal6524_proxy0.pcal6524.output_port_2 = "0x29"
   Property  16: pcal6524_proxy0.pcal6524.reserved01 = "0xc0"
   Property  17: pcal6524_proxy0.pcal6524.polarity_inv_port_0 = "0x0"
   Property  18: pcal6524_proxy0.pcal6524.polarity_inv_port_1 = "0x0"
   Property  19: pcal6524_proxy0.pcal6524.polarity_inv_port_2 = "0x0"
   Property  20: pcal6524_proxy0.pcal6524.reserved02 = "0xc0"
   Property  21: pcal6524_proxy0.pcal6524.config_port_0 = "0x0"
   Property  22: pcal6524_proxy0.pcal6524.config_port_1 = "0x0"
   Property  23: pcal6524_proxy0.pcal6524.config_port_2 = "0x0"
   Property  24: pcal6524_proxy0.pcal6524.reserved03 = "0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0"
   Property  25: pcal6524_proxy0.pcal6524.out_drv_str_reg_0A = "0xff"
   Property  26: pcal6524_proxy0.pcal6524.out_drv_str_reg_0B = "0xff"
   Property  27: pcal6524_proxy0.pcal6524.out_drv_str_reg_1A = "0xff"
   Property  28: pcal6524_proxy0.pcal6524.out_drv_str_reg_1B = "0xff"
   Property  29: pcal6524_proxy0.pcal6524.out_drv_str_reg_2A = "0xff"
   Property  30: pcal6524_proxy0.pcal6524.out_drv_str_reg_2B = "0xff"
   Property  31: pcal6524_proxy0.pcal6524.reserved04 = "0xde,0xc0"
   Property  32: pcal6524_proxy0.pcal6524.input_latch_reg_0 = "0x0"
   Property  33: pcal6524_proxy0.pcal6524.input_latch_reg_1 = "0x0"
   Property  34: pcal6524_proxy0.pcal6524.input_latch_reg_2 = "0x0"
   Property  35: pcal6524_proxy0.pcal6524.reserved05 = "0xc0"
   Property  36: pcal6524_proxy0.pcal6524.pup_pdown_en_reg_0 = "0x0"
   Property  37: pcal6524_proxy0.pcal6524.pup_pdown_en_reg_1 = "0x0"
   Property  38: pcal6524_proxy0.pcal6524.pup_pdown_en_reg_2 = "0x0"
   Property  39: pcal6524_proxy0.pcal6524.reserved06 = "0xc0"
   Property  40: pcal6524_proxy0.pcal6524.pup_pdown_sel_reg_0 = "0xff"
   Property  41: pcal6524_proxy0.pcal6524.pup_pdown_sel_reg_1 = "0xff"
   Property  42: pcal6524_proxy0.pcal6524.pup_pdown_sel_reg_2 = "0xff"
   Property  43: pcal6524_proxy0.pcal6524.reserved07 = "0xc0"
   Property  44: pcal6524_proxy0.pcal6524.interrupt_mask_reg_0 = "0xff"
   Property  45: pcal6524_proxy0.pcal6524.interrupt_mask_reg_1 = "0xff"
   Property  46: pcal6524_proxy0.pcal6524.interrupt_mask_reg_2 = "0xff"
   Property  47: pcal6524_proxy0.pcal6524.reserved08 = "0xc0"
   Property  48: pcal6524_proxy0.pcal6524.interrupt_status_reg_0 = "0x0"
   Property  49: pcal6524_proxy0.pcal6524.interrupt_status_reg_1 = "0x0"
   Property  50: pcal6524_proxy0.pcal6524.interrupt_status_reg_2 = "0x0"
   Property  51: pcal6524_proxy0.pcal6524.reserved09 = "0xc0"
   Property  52: pcal6524_proxy0.pcal6524.out_port_config_reg = "0x0"
   Property  53: pcal6524_proxy0.pcal6524.reserved10 = "0x42,0xde,0xc0"
   Property  54: pcal6524_proxy0.pcal6524.interrupt_edge_reg_0A = "0x0"
   Property  55: pcal6524_proxy0.pcal6524.interrupt_edge_reg_0B = "0x0"
   Property  56: pcal6524_proxy0.pcal6524.interrupt_edge_reg_1A = "0x0"
   Property  57: pcal6524_proxy0.pcal6524.interrupt_edge_reg_1B = "0x0"
   Property  58: pcal6524_proxy0.pcal6524.interrupt_edge_reg_2A = "0x0"
   Property  59: pcal6524_proxy0.pcal6524.interrupt_edge_reg_2B = "0x0"
   Property  60: pcal6524_proxy0.pcal6524.reserved11 = "0xde,0xc0"
   Property  61: pcal6524_proxy0.pcal6524.interrupt_clear_reg_0 = "0x0" (cached)
   Property  62: pcal6524_proxy0.pcal6524.interrupt_clear_reg_1 = "0x0" (cached)
   Property  63: pcal6524_proxy0.pcal6524.interrupt_clear_reg_2 = "0x0" (cached)
   Property  64: pcal6524_proxy0.pcal6524.reserved12 = "0xc0"
   Property  65: pcal6524_proxy0.pcal6524.input_status_port_0 = "0x51"
   Property  66: pcal6524_proxy0.pcal6524.input_status_port_1 = "0x0"
   Property  67: pcal6524_proxy0.pcal6524.input_status_port_2 = "0x29"
   Property  68: pcal6524_proxy0.pcal6524.reserved13 = "0xc0"
   Property  69: pcal6524_proxy0.pcal6524.indv_pin_out_cnfg_reg_0 = "0x0"
   Property  70: pcal6524_proxy0.pcal6524.indv_pin_out_cnfg_reg_1 = "0x0"
   Property  71: pcal6524_proxy0.pcal6524.indv_pin_out_cnfg_reg_2 = "0x0"
   Property  72: pcal6524_proxy0.pcal6524.reserved14 = "0xc0"
   Property  73: pcal6524_proxy0.pcal6524.switch_debounce_en_0 = "0x0"
   Property  74: pcal6524_proxy0.pcal6524.switch_debounce_en_1 = "0x0"
   Property  75: pcal6524_proxy0.pcal6524.switch_debounce_count = "0x0"
   Property  76: pcal6524_proxy0.pcal6524.reserved15 = "0xc0,0x2,0x42,0xde,0xc0,0x2,0x42,0xde,0xc0"
   Property  80: pcal6524_proxy1.pcal6524.input_port_0 = "0x3"
   Property  81: pcal6524_proxy1.pcal6524.input_port_1 = "0x42"
   Property  82: pcal6524_proxy1.pcal6524.input_port_2 = "0xde"
   Property  83: pcal6524_proxy1.pcal6524.reserved00 = "0xc0"
   Property  84: pcal6524_proxy1.pcal6524.output_port_0 = "0x3"
   Property  85: pcal6524_proxy1.pcal6524.output_port_1 = "0x42"
   Property  86: pcal6524_proxy1.pcal6524.output_port_2 = "0xde"
   Property  87: pcal6524_proxy1.pcal6524.reserved01 = "0xc0"
   Property  88: pcal6524_proxy1.pcal6524.polarity_inv_port_0 = "0x3"
   Property  89: pcal6524_proxy1.pcal6524.polarity_inv_port_1 = "0x42"
   Property  90: pcal6524_proxy1.pcal6524.polarity_inv_port_2 = "0xde"
   Property  91: pcal6524_proxy1.pcal6524.reserved02 = "0xc0"
   Property  92: pcal6524_proxy1.pcal6524.config_port_0 = "0x3"
   Property  93: pcal6524_proxy1.pcal6524.config_port_1 = "0x42"
   Property  94: pcal6524_proxy1.pcal6524.config_port_2 = "0xde"
   Property  95: pcal6524_proxy1.pcal6524.reserved03 = "0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0"
   Property  96: pcal6524_proxy1.pcal6524.out_drv_str_reg_0A = "0x3"
   Property  97: pcal6524_proxy1.pcal6524.out_drv_str_reg_0B = "0x42"
   Property  98: pcal6524_proxy1.pcal6524.out_drv_str_reg_1A = "0xde"
   Property  99: pcal6524_proxy1.pcal6524.out_drv_str_reg_1B = "0xc0"
   Property 100: pcal6524_proxy1.pcal6524.out_drv_str_reg_2A = "0x3"
   Property 101: pcal6524_proxy1.pcal6524.out_drv_str_reg_2B = "0x42"
   Property 102: pcal6524_proxy1.pcal6524.reserved04 = "0xde,0xc0"
   Property 103: pcal6524_proxy1.pcal6524.input_latch_reg_0 = "0x3"
   Property 104: pcal6524_proxy1.pcal6524.input_latch_reg_1 = "0x42"
   Property 105: pcal6524_proxy1.pcal6524.input_latch_reg_2 = "0xde"
   Property 106: pcal6524_proxy1.pcal6524.reserved05 = "0xc0"
   Property 107: pcal6524_proxy1.pcal6524.pup_pdown_en_reg_0 = "0x3"
   Property 108: pcal6524_proxy1.pcal6524.pup_pdown_en_reg_1 = "0x42"
   Property 109: pcal6524_proxy1.pcal6524.pup_pdown_en_reg_2 = "0xde"
   Property 110: pcal6524_proxy1.pcal6524.reserved06 = "0xc0"
   Property 111: pcal6524_proxy1.pcal6524.pup_pdown_sel_reg_0 = "0x3"
   Property 112: pcal6524_proxy1.pcal6524.pup_pdown_sel_reg_1 = "0x42"
   Property 113: pcal6524_proxy1.pcal6524.pup_pdown_sel_reg_2 = "0xde"
   Property 114: pcal6524_proxy1.pcal6524.reserved07 = "0xc0"
   Property 115: pcal6524_proxy1.pcal6524.interrupt_mask_reg_0 = "0x3"
   Property 116: pcal6524_proxy1.pcal6524.interrupt_mask_reg_1 = "0x42"
   Property 117: pcal6524_proxy1.pcal6524.interrupt_mask_reg_2 = "0xde"
   Property 118: pcal6524_proxy1.pcal6524.reserved08 = "0xc0"
   Property 119: pcal6524_proxy1.pcal6524.interrupt_status_reg_0 = "0x3"
   Property 120: pcal6524_proxy1.pcal6524.interrupt_status_reg_1 = "0x42"
   Property 121: pcal6524_proxy1.pcal6524.interrupt_status_reg_2 = "0xde"
   Property 122: pcal6524_proxy1.pcal6524.reserved09 = "0xc0"
   Property 123: pcal6524_proxy1.pcal6524.out_port_config_reg = "0x3"
   Property 124: pcal6524_proxy1.pcal6524.reserved10 = "0x42,0xde,0xc0"
   Property 125: pcal6524_proxy1.pcal6524.interrupt_edge_reg_0A = "0x3"
   Property 126: pcal6524_proxy1.pcal6524.interrupt_edge_reg_0B = "0x42"
   Property 127: pcal6524_proxy1.pcal6524.interrupt_edge_reg_1A = "0xde"
   Property 128: pcal6524_proxy1.pcal6524.interrupt_edge_reg_1B = "0xc0"
   Property 129: pcal6524_proxy1.pcal6524.interrupt_edge_reg_2A = "0x3"
   Property 130: pcal6524_proxy1.pcal6524.interrupt_edge_reg_2B = "0x42"
   Property 131: pcal6524_proxy1.pcal6524.reserved11 = "0xde,0xc0"
   Property 132: pcal6524_proxy1.pcal6524.interrupt_clear_reg_0 = "0x0" (cached)
   Property 133: pcal6524_proxy1.pcal6524.interrupt_clear_reg_1 = "0x0" (cached)
   Property 134: pcal6524_proxy1.pcal6524.interrupt_clear_reg_2 = "0x0" (cached)
   Property 135: pcal6524_proxy1.pcal6524.reserved12 = "0xc0"
   Property 136: pcal6524_proxy1.pcal6524.input_status_port_0 = "0x3"
   Property 137: pcal6524_proxy1.pcal6524.input_status_port_1 = "0x42"
   Property 138: pcal6524_proxy1.pcal6524.input_status_port_2 = "0xde"
   Property 139: pcal6524_proxy1.pcal6524.reserved13 = "0xc0"
   Property 140: pcal6524_proxy1.pcal6524.indv_pin_out_cnfg_reg_0 = "0x3"
   Property 141: pcal6524_proxy1.pcal6524.indv_pin_out_cnfg_reg_1 = "0x42"
   Property 142: pcal6524_proxy1.pcal6524.indv_pin_out_cnfg_reg_2 = "0xde"
   Property 143: pcal6524_proxy1.pcal6524.reserved14 = "0xc0"
   Property 144: pcal6524_proxy1.pcal6524.switch_debounce_en_0 = "0x3"
   Property 145: pcal6524_proxy1.pcal6524.switch_debounce_en_1 = "0x42"
   Property 146: pcal6524_proxy1.pcal6524.switch_debounce_count = "0xde"
   Property 147: pcal6524_proxy1.pcal6524.reserved15 = "0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0"
   Property 151: pcal6524_proxy2.pcal6524.input_port_0 = "0x3"
   Property 152: pcal6524_proxy2.pcal6524.input_port_1 = "0x42"
   Property 153: pcal6524_proxy2.pcal6524.input_port_2 = "0xde"
   Property 154: pcal6524_proxy2.pcal6524.reserved00 = "0xc0"
   Property 155: pcal6524_proxy2.pcal6524.output_port_0 = "0x3"
   Property 156: pcal6524_proxy2.pcal6524.output_port_1 = "0x42"
   Property 157: pcal6524_proxy2.pcal6524.output_port_2 = "0xde"
   Property 158: pcal6524_proxy2.pcal6524.reserved01 = "0xc0"
   Property 159: pcal6524_proxy2.pcal6524.polarity_inv_port_0 = "0x3"
   Property 160: pcal6524_proxy2.pcal6524.polarity_inv_port_1 = "0x42"
   Property 161: pcal6524_proxy2.pcal6524.polarity_inv_port_2 = "0xde"
   Property 162: pcal6524_proxy2.pcal6524.reserved02 = "0xc0"
   Property 163: pcal6524_proxy2.pcal6524.config_port_0 = "0x3"
   Property 164: pcal6524_proxy2.pcal6524.config_port_1 = "0x42"
   Property 165: pcal6524_proxy2.pcal6524.config_port_2 = "0xde"
   Property 166: pcal6524_proxy2.pcal6524.reserved03 = "0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0"
   Property 167: pcal6524_proxy2.pcal6524.out_drv_str_reg_0A = "0x3"
   Property 168: pcal6524_proxy2.pcal6524.out_drv_str_reg_0B = "0x42"
   Property 169: pcal6524_proxy2.pcal6524.out_drv_str_reg_1A = "0xde"
   Property 170: pcal6524_proxy2.pcal6524.out_drv_str_reg_1B = "0xc0"
   Property 171: pcal6524_proxy2.pcal6524.out_drv_str_reg_2A = "0x3"
   Property 172: pcal6524_proxy2.pcal6524.out_drv_str_reg_2B = "0x42"
   Property 173: pcal6524_proxy2.pcal6524.reserved04 = "0xde,0xc0"
   Property 174: pcal6524_proxy2.pcal6524.input_latch_reg_0 = "0x3"
   Property 175: pcal6524_proxy2.pcal6524.input_latch_reg_1 = "0x42"
   Property 176: pcal6524_proxy2.pcal6524.input_latch_reg_2 = "0xde"
   Property 177: pcal6524_proxy2.pcal6524.reserved05 = "0xc0"
   Property 178: pcal6524_proxy2.pcal6524.pup_pdown_en_reg_0 = "0x3"
   Property 179: pcal6524_proxy2.pcal6524.pup_pdown_en_reg_1 = "0x42"
   Property 180: pcal6524_proxy2.pcal6524.pup_pdown_en_reg_2 = "0xde"
   Property 181: pcal6524_proxy2.pcal6524.reserved06 = "0xc0"
   Property 182: pcal6524_proxy2.pcal6524.pup_pdown_sel_reg_0 = "0x3"
   Property 183: pcal6524_proxy2.pcal6524.pup_pdown_sel_reg_1 = "0x42"
   Property 184: pcal6524_proxy2.pcal6524.pup_pdown_sel_reg_2 = "0xde"
   Property 185: pcal6524_proxy2.pcal6524.reserved07 = "0xc0"
   Property 186: pcal6524_proxy2.pcal6524.interrupt_mask_reg_0 = "0x3"
   Property 187: pcal6524_proxy2.pcal6524.interrupt_mask_reg_1 = "0x42"
   Property 188: pcal6524_proxy2.pcal6524.interrupt_mask_reg_2 = "0xde"
   Property 189: pcal6524_proxy2.pcal6524.reserved08 = "0xc0"
   Property 190: pcal6524_proxy2.pcal6524.interrupt_status_reg_0 = "0x3"
   Property 191: pcal6524_proxy2.pcal6524.interrupt_status_reg_1 = "0x42"
   Property 192: pcal6524_proxy2.pcal6524.interrupt_status_reg_2 = "0xde"
   Property 193: pcal6524_proxy2.pcal6524.reserved09 = "0xc0"
   Property 194: pcal6524_proxy2.pcal6524.out_port_config_reg = "0x3"
   Property 195: pcal6524_proxy2.pcal6524.reserved10 = "0x42,0xde,0xc0"
   Property 196: pcal6524_proxy2.pcal6524.interrupt_edge_reg_0A = "0x3"
   Property 197: pcal6524_proxy2.pcal6524.interrupt_edge_reg_0B = "0x42"
   Property 198: pcal6524_proxy2.pcal6524.interrupt_edge_reg_1A = "0xde"
   Property 199: pcal6524_proxy2.pcal6524.interrupt_edge_reg_1B = "0xc0"
   Property 200: pcal6524_proxy2.pcal6524.interrupt_edge_reg_2A = "0x3"
   Property 201: pcal6524_proxy2.pcal6524.interrupt_edge_reg_2B = "0x42"
   Property 202: pcal6524_proxy2.pcal6524.reserved11 = "0xde,0xc0"
   Property 203: pcal6524_proxy2.pcal6524.interrupt_clear_reg_0 = "0x0" (cached)
   Property 204: pcal6524_proxy2.pcal6524.interrupt_clear_reg_1 = "0x0" (cached)
   Property 205: pcal6524_proxy2.pcal6524.interrupt_clear_reg_2 = "0x0" (cached)
   Property 206: pcal6524_proxy2.pcal6524.reserved12 = "0xc0"
   Property 207: pcal6524_proxy2.pcal6524.input_status_port_0 = "0x3"
   Property 208: pcal6524_proxy2.pcal6524.input_status_port_1 = "0x42"
   Property 209: pcal6524_proxy2.pcal6524.input_status_port_2 = "0xde"
   Property 210: pcal6524_proxy2.pcal6524.reserved13 = "0xc0"
   Property 211: pcal6524_proxy2.pcal6524.indv_pin_out_cnfg_reg_0 = "0x3"
   Property 212: pcal6524_proxy2.pcal6524.indv_pin_out_cnfg_reg_1 = "0x42"
   Property 213: pcal6524_proxy2.pcal6524.indv_pin_out_cnfg_reg_2 = "0xde"
   Property 214: pcal6524_proxy2.pcal6524.reserved14 = "0xc0"
   Property 215: pcal6524_proxy2.pcal6524.switch_debounce_en_0 = "0x3"
   Property 216: pcal6524_proxy2.pcal6524.switch_debounce_en_1 = "0x42"
   Property 217: pcal6524_proxy2.pcal6524.switch_debounce_count = "0xde"
   Property 218: pcal6524_proxy2.pcal6524.reserved15 = "0xc0,0x3,0x42,0xde,0xc0,0x3,0x42,0xde,0xc0"
   Application started/running [0 s 834 ms]
   [1 s 5 ms]
   Dump of all final property values:
   Property   9: pcal6524_proxy0.pcal6524.input_port_0 = "0x51"
   Property  10: pcal6524_proxy0.pcal6524.input_port_1 = "0x0"
   Property  11: pcal6524_proxy0.pcal6524.input_port_2 = "0x29"
   Property  13: pcal6524_proxy0.pcal6524.output_port_0 = "0x51"
   Property  14: pcal6524_proxy0.pcal6524.output_port_1 = "0x0"
   Property  15: pcal6524_proxy0.pcal6524.output_port_2 = "0x29"
   Property  17: pcal6524_proxy0.pcal6524.polarity_inv_port_0 = "0x0"
   Property  18: pcal6524_proxy0.pcal6524.polarity_inv_port_1 = "0x0"
   Property  19: pcal6524_proxy0.pcal6524.polarity_inv_port_2 = "0x0"
   Property  21: pcal6524_proxy0.pcal6524.config_port_0 = "0x0"
   Property  22: pcal6524_proxy0.pcal6524.config_port_1 = "0x0"
   Property  23: pcal6524_proxy0.pcal6524.config_port_2 = "0x0"
   Property  25: pcal6524_proxy0.pcal6524.out_drv_str_reg_0A = "0xff"
   Property  26: pcal6524_proxy0.pcal6524.out_drv_str_reg_0B = "0xff"
   Property  27: pcal6524_proxy0.pcal6524.out_drv_str_reg_1A = "0xff"
   Property  28: pcal6524_proxy0.pcal6524.out_drv_str_reg_1B = "0xff"
   Property  29: pcal6524_proxy0.pcal6524.out_drv_str_reg_2A = "0xff"
   Property  30: pcal6524_proxy0.pcal6524.out_drv_str_reg_2B = "0xff"
   Property  32: pcal6524_proxy0.pcal6524.input_latch_reg_0 = "0x0"
   Property  33: pcal6524_proxy0.pcal6524.input_latch_reg_1 = "0x0"
   Property  34: pcal6524_proxy0.pcal6524.input_latch_reg_2 = "0x0"
   Property  36: pcal6524_proxy0.pcal6524.pup_pdown_en_reg_0 = "0x0"
   Property  37: pcal6524_proxy0.pcal6524.pup_pdown_en_reg_1 = "0x0"
   Property  38: pcal6524_proxy0.pcal6524.pup_pdown_en_reg_2 = "0x0"
   Property  40: pcal6524_proxy0.pcal6524.pup_pdown_sel_reg_0 = "0xff"
   Property  41: pcal6524_proxy0.pcal6524.pup_pdown_sel_reg_1 = "0xff"
   Property  42: pcal6524_proxy0.pcal6524.pup_pdown_sel_reg_2 = "0xff"
   Property  44: pcal6524_proxy0.pcal6524.interrupt_mask_reg_0 = "0xff"
   Property  45: pcal6524_proxy0.pcal6524.interrupt_mask_reg_1 = "0xff"
   Property  46: pcal6524_proxy0.pcal6524.interrupt_mask_reg_2 = "0xff"
   Property  48: pcal6524_proxy0.pcal6524.interrupt_status_reg_0 = "0x0"
   Property  49: pcal6524_proxy0.pcal6524.interrupt_status_reg_1 = "0x0"
   Property  50: pcal6524_proxy0.pcal6524.interrupt_status_reg_2 = "0x0"
   Property  52: pcal6524_proxy0.pcal6524.out_port_config_reg = "0x0"
   Property  54: pcal6524_proxy0.pcal6524.interrupt_edge_reg_0A = "0x0"
   Property  55: pcal6524_proxy0.pcal6524.interrupt_edge_reg_0B = "0x0"
   Property  56: pcal6524_proxy0.pcal6524.interrupt_edge_reg_1A = "0x0"
   Property  57: pcal6524_proxy0.pcal6524.interrupt_edge_reg_1B = "0x0"
   Property  58: pcal6524_proxy0.pcal6524.interrupt_edge_reg_2A = "0x0"
   Property  59: pcal6524_proxy0.pcal6524.interrupt_edge_reg_2B = "0x0"
   Property  61: pcal6524_proxy0.pcal6524.interrupt_clear_reg_0 = "0x0" (cached)
   Property  62: pcal6524_proxy0.pcal6524.interrupt_clear_reg_1 = "0x0" (cached)
   Property  63: pcal6524_proxy0.pcal6524.interrupt_clear_reg_2 = "0x0" (cached)
   Property  65: pcal6524_proxy0.pcal6524.input_status_port_0 = "0x51"
   Property  66: pcal6524_proxy0.pcal6524.input_status_port_1 = "0x0"
   Property  67: pcal6524_proxy0.pcal6524.input_status_port_2 = "0x29"
   Property  69: pcal6524_proxy0.pcal6524.indv_pin_out_cnfg_reg_0 = "0x0"
   Property  70: pcal6524_proxy0.pcal6524.indv_pin_out_cnfg_reg_1 = "0x0"
   Property  71: pcal6524_proxy0.pcal6524.indv_pin_out_cnfg_reg_2 = "0x0"
   Property  73: pcal6524_proxy0.pcal6524.switch_debounce_en_0 = "0x0"
   Property  74: pcal6524_proxy0.pcal6524.switch_debounce_en_1 = "0x0"
   Property  75: pcal6524_proxy0.pcal6524.switch_debounce_count = "0x0"
   Property  80: pcal6524_proxy1.pcal6524.input_port_0 = "0x0"
   Property  81: pcal6524_proxy1.pcal6524.input_port_1 = "0x0"
   Property  82: pcal6524_proxy1.pcal6524.input_port_2 = "0x0"
   Property  84: pcal6524_proxy1.pcal6524.output_port_0 = "0x0"
   Property  85: pcal6524_proxy1.pcal6524.output_port_1 = "0x0"
   Property  86: pcal6524_proxy1.pcal6524.output_port_2 = "0x0"
   Property  88: pcal6524_proxy1.pcal6524.polarity_inv_port_0 = "0x0"
   Property  89: pcal6524_proxy1.pcal6524.polarity_inv_port_1 = "0x0"
   Property  90: pcal6524_proxy1.pcal6524.polarity_inv_port_2 = "0x0"
   Property  92: pcal6524_proxy1.pcal6524.config_port_0 = "0x0"
   Property  93: pcal6524_proxy1.pcal6524.config_port_1 = "0x0"
   Property  94: pcal6524_proxy1.pcal6524.config_port_2 = "0x0"
   Property  96: pcal6524_proxy1.pcal6524.out_drv_str_reg_0A = "0xff"
   Property  97: pcal6524_proxy1.pcal6524.out_drv_str_reg_0B = "0xff"
   Property  98: pcal6524_proxy1.pcal6524.out_drv_str_reg_1A = "0xff"
   Property  99: pcal6524_proxy1.pcal6524.out_drv_str_reg_1B = "0xff"
   Property 100: pcal6524_proxy1.pcal6524.out_drv_str_reg_2A = "0xff"
   Property 101: pcal6524_proxy1.pcal6524.out_drv_str_reg_2B = "0xff"
   Property 103: pcal6524_proxy1.pcal6524.input_latch_reg_0 = "0x0"
   Property 104: pcal6524_proxy1.pcal6524.input_latch_reg_1 = "0x0"
   Property 105: pcal6524_proxy1.pcal6524.input_latch_reg_2 = "0x0"
   Property 107: pcal6524_proxy1.pcal6524.pup_pdown_en_reg_0 = "0x0"
   Property 108: pcal6524_proxy1.pcal6524.pup_pdown_en_reg_1 = "0x0"
   Property 109: pcal6524_proxy1.pcal6524.pup_pdown_en_reg_2 = "0x0"
   Property 111: pcal6524_proxy1.pcal6524.pup_pdown_sel_reg_0 = "0xff"
   Property 112: pcal6524_proxy1.pcal6524.pup_pdown_sel_reg_1 = "0xff"
   Property 113: pcal6524_proxy1.pcal6524.pup_pdown_sel_reg_2 = "0xff"
   Property 115: pcal6524_proxy1.pcal6524.interrupt_mask_reg_0 = "0xff"
   Property 116: pcal6524_proxy1.pcal6524.interrupt_mask_reg_1 = "0xff"
   Property 117: pcal6524_proxy1.pcal6524.interrupt_mask_reg_2 = "0xff"
   Property 119: pcal6524_proxy1.pcal6524.interrupt_status_reg_0 = "0x0"
   Property 120: pcal6524_proxy1.pcal6524.interrupt_status_reg_1 = "0x0"
   Property 121: pcal6524_proxy1.pcal6524.interrupt_status_reg_2 = "0x0"
   Property 123: pcal6524_proxy1.pcal6524.out_port_config_reg = "0x0"
   Property 125: pcal6524_proxy1.pcal6524.interrupt_edge_reg_0A = "0x0"
   Property 126: pcal6524_proxy1.pcal6524.interrupt_edge_reg_0B = "0x0"
   Property 127: pcal6524_proxy1.pcal6524.interrupt_edge_reg_1A = "0x0"
   Property 128: pcal6524_proxy1.pcal6524.interrupt_edge_reg_1B = "0x0"
   Property 129: pcal6524_proxy1.pcal6524.interrupt_edge_reg_2A = "0x0"
   Property 130: pcal6524_proxy1.pcal6524.interrupt_edge_reg_2B = "0x0"
   Property 132: pcal6524_proxy1.pcal6524.interrupt_clear_reg_0 = "0x0" (cached)
   Property 133: pcal6524_proxy1.pcal6524.interrupt_clear_reg_1 = "0x0" (cached)
   Property 134: pcal6524_proxy1.pcal6524.interrupt_clear_reg_2 = "0x0" (cached)
   Property 136: pcal6524_proxy1.pcal6524.input_status_port_0 = "0x0"
   Property 137: pcal6524_proxy1.pcal6524.input_status_port_1 = "0x0"
   Property 138: pcal6524_proxy1.pcal6524.input_status_port_2 = "0x0"
   Property 140: pcal6524_proxy1.pcal6524.indv_pin_out_cnfg_reg_0 = "0x0"
   Property 141: pcal6524_proxy1.pcal6524.indv_pin_out_cnfg_reg_1 = "0x0"
   Property 142: pcal6524_proxy1.pcal6524.indv_pin_out_cnfg_reg_2 = "0x0"
   Property 144: pcal6524_proxy1.pcal6524.switch_debounce_en_0 = "0x0"
   Property 145: pcal6524_proxy1.pcal6524.switch_debounce_en_1 = "0x0"
   Property 146: pcal6524_proxy1.pcal6524.switch_debounce_count = "0x0"
   Property 151: pcal6524_proxy2.pcal6524.input_port_0 = "0xda"
   Property 152: pcal6524_proxy2.pcal6524.input_port_1 = "0x0"
   Property 153: pcal6524_proxy2.pcal6524.input_port_2 = "0x82"
   Property 155: pcal6524_proxy2.pcal6524.output_port_0 = "0xda"
   Property 156: pcal6524_proxy2.pcal6524.output_port_1 = "0x0"
   Property 157: pcal6524_proxy2.pcal6524.output_port_2 = "0x82"
   Property 159: pcal6524_proxy2.pcal6524.polarity_inv_port_0 = "0x0"
   Property 160: pcal6524_proxy2.pcal6524.polarity_inv_port_1 = "0x0"
   Property 161: pcal6524_proxy2.pcal6524.polarity_inv_port_2 = "0x0"
   Property 163: pcal6524_proxy2.pcal6524.config_port_0 = "0x0"
   Property 164: pcal6524_proxy2.pcal6524.config_port_1 = "0x0"
   Property 165: pcal6524_proxy2.pcal6524.config_port_2 = "0x0"
   Property 167: pcal6524_proxy2.pcal6524.out_drv_str_reg_0A = "0xff"
   Property 168: pcal6524_proxy2.pcal6524.out_drv_str_reg_0B = "0xff"
   Property 169: pcal6524_proxy2.pcal6524.out_drv_str_reg_1A = "0xff"
   Property 170: pcal6524_proxy2.pcal6524.out_drv_str_reg_1B = "0xff"
   Property 171: pcal6524_proxy2.pcal6524.out_drv_str_reg_2A = "0xff"
   Property 172: pcal6524_proxy2.pcal6524.out_drv_str_reg_2B = "0xff"
   Property 174: pcal6524_proxy2.pcal6524.input_latch_reg_0 = "0x0"
   Property 175: pcal6524_proxy2.pcal6524.input_latch_reg_1 = "0x0"
   Property 176: pcal6524_proxy2.pcal6524.input_latch_reg_2 = "0x0"
   Property 178: pcal6524_proxy2.pcal6524.pup_pdown_en_reg_0 = "0x0"
   Property 179: pcal6524_proxy2.pcal6524.pup_pdown_en_reg_1 = "0x0"
   Property 180: pcal6524_proxy2.pcal6524.pup_pdown_en_reg_2 = "0x0"
   Property 182: pcal6524_proxy2.pcal6524.pup_pdown_sel_reg_0 = "0xff"
   Property 183: pcal6524_proxy2.pcal6524.pup_pdown_sel_reg_1 = "0xff"
   Property 184: pcal6524_proxy2.pcal6524.pup_pdown_sel_reg_2 = "0xff"
   Property 186: pcal6524_proxy2.pcal6524.interrupt_mask_reg_0 = "0xff"
   Property 187: pcal6524_proxy2.pcal6524.interrupt_mask_reg_1 = "0xff"
   Property 188: pcal6524_proxy2.pcal6524.interrupt_mask_reg_2 = "0xff"
   Property 190: pcal6524_proxy2.pcal6524.interrupt_status_reg_0 = "0x0"
   Property 191: pcal6524_proxy2.pcal6524.interrupt_status_reg_1 = "0x0"
   Property 192: pcal6524_proxy2.pcal6524.interrupt_status_reg_2 = "0x0"
   Property 194: pcal6524_proxy2.pcal6524.out_port_config_reg = "0x0"
   Property 196: pcal6524_proxy2.pcal6524.interrupt_edge_reg_0A = "0x0"
   Property 197: pcal6524_proxy2.pcal6524.interrupt_edge_reg_0B = "0x0"
   Property 198: pcal6524_proxy2.pcal6524.interrupt_edge_reg_1A = "0x0"
   Property 199: pcal6524_proxy2.pcal6524.interrupt_edge_reg_1B = "0x0"
   Property 200: pcal6524_proxy2.pcal6524.interrupt_edge_reg_2A = "0x0"
   Property 201: pcal6524_proxy2.pcal6524.interrupt_edge_reg_2B = "0x0"
   Property 203: pcal6524_proxy2.pcal6524.interrupt_clear_reg_0 = "0x0" (cached)
   Property 204: pcal6524_proxy2.pcal6524.interrupt_clear_reg_1 = "0x0" (cached)
   Property 205: pcal6524_proxy2.pcal6524.interrupt_clear_reg_2 = "0x0" (cached)
   Property 207: pcal6524_proxy2.pcal6524.input_status_port_0 = "0xda"
   Property 208: pcal6524_proxy2.pcal6524.input_status_port_1 = "0x0"
   Property 209: pcal6524_proxy2.pcal6524.input_status_port_2 = "0x82"
   Property 211: pcal6524_proxy2.pcal6524.indv_pin_out_cnfg_reg_0 = "0x0"
   Property 212: pcal6524_proxy2.pcal6524.indv_pin_out_cnfg_reg_1 = "0x0"
   Property 213: pcal6524_proxy2.pcal6524.indv_pin_out_cnfg_reg_2 = "0x0"
   Property 215: pcal6524_proxy2.pcal6524.switch_debounce_en_0 = "0x0"
   Property 216: pcal6524_proxy2.pcal6524.switch_debounce_en_1 = "0x0"
   Property 217: pcal6524_proxy2.pcal6524.switch_debounce_count = "0x0"

..
