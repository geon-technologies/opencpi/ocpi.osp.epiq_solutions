.. fsk_dig_radio_ctrlr_csts application documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. Company:     Geon Technologies, LLC
   Author:      AP
   Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
                Dissemination of this information or reproduction of this
                material is strictly prohibited unless prior written
                permission is obtained from Geon Technologies, LLC

:orphan:

.. _fsk_dig_radio_ctrlr_csts:

app: ``fsk_dig_radio_ctrlr_csts``
=====================================

Description
-----------

This app transfers an image (prepended with a sync pattern) into the FPGA, through the AD9361
transceiver, back into the FPGA and into the ARM. The FPGA implements an assembly of digital
signal processing modules that supports a simple FSK modulator/demodulator. Additional processing
on the received data by the ARM in which to recover the original decoded image.

Hardware Portability
^^^^^^^^^^^^^^^^^^^^

In its current state, this application requires some light modifications to be ported to another
platform.

Prerequisites
-------------

The ``z3u`` has been properly installed and deployed, and the application assembly
bitstream is built. To name a few, this test application requires that the following
assets are built:

  * HDL Assembly: ``fsk_modem_csts`` (from the Z3u OSP)
  * RCC Worker: ``file_read.rcc`` (from ``core`` project)
  * RCC Worker: ``file_write.rcc`` (from ``core`` project)

Execution
---------

The execution commands herein are subject to change. This application is ran via ``ocpirun``.

.. note::

   The instructions are written for **Network Mode**

..

#. Boot the Matchstiq Z3U, setup for Network Mode, then browse to the following directory:

   ``% cd /mnt/ocpi_z3u/applications/fsk_dig_radio_ctrlr_csts/``

#. Setup dependent on version of FOSS:

   ``% export OCPI_DMA_CACHE_MODE=0`` (REQUIRED for UltraScale+ platforms (<= v2.1.0))

#. Setup the artifact search path:

   ``% export OCPI_LIBRARY_PATH=../../hdl/assemblies/fsk_modem_csts:../../hdl/devices:/mnt/ocpi_platform/artifacts:/mnt/ocpi_assets/artifacts:/mnt/ocpi_core/artifacts``

#. Generate the FIR filter tap files

   ``% make``

   .. code-block::
     
     $ make
     No application found when looking for: fsk_dig_radio_ctrlr_csts.xml, fsk_dig_radio_ctrlr_csts.{cc,cxx,cpp}
     # Remove old test data files
     rm -rf idata/*.dat idata/*.bin odata/*
     # Create test input data
     python scripts/gen_rrcos_taps.py 128 0.95 `echo "1/6400" | bc -l` `echo "6400*39" | bc -l` 4096 idata/tx_rrcos_taps.dat

     ********************************************************************************
     *** Python: Generate Root-Raised Cosine taps ***
     1.25883658098
     [  85   96  105  111  115  116  114  109  100   88   72   53   31    6  -21
     -50  -81 -113 -145 -177 -207 -235 -259 -280 -295 -305 -307 -302 -287 -263
     -229 -184 -128  -60   20  112  217  334  462  602  752  912 1081 1257 1441
     1629 1821 2015 2209 2402 2593 2779 2958 3130 3292 3443 3581 3705 3814 3906
     3981 4038 4076 4096 4096 4076 4038 3981 3906 3814 3705 3581 3443 3292 3130
     2958 2779 2593 2402 2209 2015 1821 1629 1441 1257 1081  912  752  602  462
     334  217  112   20  -60 -128 -184 -229 -263 -287 -302 -307 -305 -295 -280
     -259 -235 -207 -177 -145 -113  -81  -50  -21    6   31   53   72   88  100
     109  114  116  115  111  105   96   85]
     127862
     31.2163085938
     tap export
     85, 96, 105, 111, 115, 116, 114, 109, 100, 88, 72, 53, 31, 6, -21, -50, -81, -113, -145, -177, -207, -235, -259, -280, -295, -305, -307, -302, -287, -263, -229, -184, -128, -60, 20, 112, 217, 334, 462, 602, 752, 912, 1081, 1257, 1441, 1629, 1821, 2015, 2209, 2402, 2593, 2779, 2958, 3130, 3292, 3443, 3581, 3705, 3814, 3906, 3981, 4038, 4076, 4096, 4096, 4076, 4038, 3981, 3906, 3814, 3705, 3581, 3443, 3292, 3130, 2958, 2779, 2593, 2402, 2209, 2015, 1821, 1629, 1441, 1257, 1081, 912, 752, 602, 462, 334, 217, 112, 20, -60, -128, -184, -229, -263, -287, -302, -307, -305, -295, -280, -259, -235, -207, -177, -145, -113, -81, -50, -21, 6, 31, 53, 72, 88, 100, 109, 114, 116, 115, 111, 105, 96, 85,
     python scripts/gen_rrcos_taps.py 128 0.95 `echo "1/6400" | bc -l` `echo "6400*39" | bc -l` 4096 idata/rx_rrcos_taps.dat

     ********************************************************************************
     *** Python: Generate Root-Raised Cosine taps ***
     1.25883658098
     [  85   96  105  111  115  116  114  109  100   88   72   53   31    6  -21
     -50  -81 -113 -145 -177 -207 -235 -259 -280 -295 -305 -307 -302 -287 -263
     -229 -184 -128  -60   20  112  217  334  462  602  752  912 1081 1257 1441
     1629 1821 2015 2209 2402 2593 2779 2958 3130 3292 3443 3581 3705 3814 3906
     3981 4038 4076 4096 4096 4076 4038 3981 3906 3814 3705 3581 3443 3292 3130
     2958 2779 2593 2402 2209 2015 1821 1629 1441 1257 1081  912  752  602  462
     334  217  112   20  -60 -128 -184 -229 -263 -287 -302 -307 -305 -295 -280
     -259 -235 -207 -177 -145 -113  -81  -50  -21    6   31   53   72   88  100
     109  114  116  115  111  105   96   85]
     127862
     31.2163085938
     tap export
     85, 96, 105, 111, 115, 116, 114, 109, 100, 88, 72, 53, 31, 6, -21, -50, -81, -113, -145, -177, -207, -235, -259, -280, -295, -305, -307, -302, -287, -263, -229, -184, -128, -60, 20, 112, 217, 334, 462, 602, 752, 912, 1081, 1257, 1441, 1629, 1821, 2015, 2209, 2402, 2593, 2779, 2958, 3130, 3292, 3443, 3581, 3705, 3814, 3906, 3981, 4038, 4076, 4096, 4096, 4076, 4038, 3981, 3906, 3814, 3705, 3581, 3443, 3292, 3130, 2958, 2779, 2593, 2402, 2209, 2015, 1821, 1629, 1441, 1257, 1081, 912, 752, 602, 462, 334, 217, 112, 20, -60, -128, -184, -229, -263, -287, -302, -307, -305, -295, -280, -259, -235, -207, -177, -145, -113, -81, -50, -21, 6, 31, 53, 72, 88, 100, 109, 114, 116, 115, 111, 105, 96, 85,

#. Run the application

   A. Using via ``ocpirun``

      ``% ocpirun -v -t 15 fsk_modem_app.xml``

      .. code-block::

	 % export OCPI_LIBRARY_PATH=../../hdl/assemblies/fsk_modem_csts:../../hdl/devices:/mnt/ocpi_platform/artifacts:/mnt/ocpi_assets/artifacts:/mnt/ocpi_core/artifacts
	 % ocpirun -v -t 15 fsk_modem_app.xml
	 Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
	 Actual deployment is:
	 Instance  0 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in /mnt/ocpi_core/artifacts/ocpi.core.file_read.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:08:09 2021
	 Instance  1 mfsk_mapper (spec ocpi.assets.comms_comps.mfsk_mapper) on hdl container 0: PL:0, using mfsk_mapper/a/mfsk_mapper in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance  2 zero_pad (spec ocpi.assets.util_comps.zero_pad) on hdl container 0: PL:0, using zero_pad-1/a/zero_pad in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance  3 tx_fir_real (spec ocpi.assets.dsp_comps.fir_real_sse) on hdl container 0: PL:0, using fir_real_sse/a/tx_fir_real in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance  4 phase_to_amp_cordic (spec ocpi.assets.dsp_comps.phase_to_amp_cordic) on hdl container 0: PL:0, using phase_to_amp_cordic-1/a/phase_to_amp_cordic in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance  5 rp_cordic (spec ocpi.assets.dsp_comps.rp_cordic) on hdl container 0: PL:0, using rp_cordic/a/rp_cordic in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance  6 rx_fir_real (spec ocpi.assets.dsp_comps.fir_real_sse) on hdl container 0: PL:0, using fir_real_sse/a/rx_fir_real in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance  7 baudTracking (spec ocpi.assets.dsp_comps.baudTracking) on rcc container 1: rcc0, using Baudtracking_simple in /mnt/ocpi_assets/artifacts/ocpi.assets.dsp_comps.Baudtracking_simple.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:11:53 2021
	 Instance  8 real_digitizer (spec ocpi.assets.dsp_comps.real_digitizer) on rcc container 1: rcc0, using real_digitizer in /mnt/ocpi_assets/artifacts/ocpi.assets.dsp_comps.real_digitizer.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:11:49 2021
	 Instance  9 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in /mnt/ocpi_core/artifacts/ocpi.core.file_write.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:08:36 2021
	 Instance 10 drc (spec ocpi.platform.drc) on rcc container 1: rcc0, using drc_z3u_mode_2_cmos_csts in ../../hdl/devices/drc_z3u_mode_2_cmos_csts.rcc/target-xilinx18_3_aarch64/drc_z3u_mode_2_cmos_csts.so dated Wed Jan 12 18:14:24 2022
	 Instance 11 drc.config (spec ocpi.platform.devices.platform_ad9361_config_csts) on hdl container 0: PL:0, using platform_ad9361_config_csts/c/platform_ad9361_config_csts in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance 12 drc.data_sub (spec ocpi.platform.devices.platform_csts_ad9361_data_sub) on hdl container 0: PL:0, using platform_csts_ad9361_data_sub-3/c/platform_csts_ad9361_data_sub in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance 13 drc.rx_qadc0 (spec ocpi.platform.devices.data_src_qadc_csts) on hdl container 0: PL:0, using data_src_qadc_csts-1/c/data_src_qadc_csts0 in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance 14 drc.rx_csts_to_iqstream0 (spec ocpi.assets.misc_comps.csts_to_iqstream) on hdl container 0: PL:0, using csts_to_iqstream/a/csts_to_iqstream in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance 15 drc.rx_complex_mixer0 (spec ocpi.assets.dsp_comps.complex_mixer) on hdl container 0: PL:0, using complex_mixer/a/complex_mixer in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance 16 drc.rx_cic_dec0 (spec ocpi.assets.dsp_comps.cic_dec) on hdl container 0: PL:0, using cic_dec-5/a/cic_dec in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance 17 drc.tx_cic_int0 (spec ocpi.assets.dsp_comps.cic_int) on hdl container 0: PL:0, using cic_int-5/a/cic_int in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance 18 drc.tx_iqstream_to_csts0 (spec ocpi.assets.misc_comps.iqstream_to_csts) on hdl container 0: PL:0, using iqstream_to_csts/a/iqstream_to_csts in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance 19 drc.tx_qdac0 (spec ocpi.platform.devices.data_sink_qdac_csts) on hdl container 0: PL:0, using data_sink_qdac_csts/c/data_sink_qdac_csts in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Instance 20 drc.ad9361_spi (spec ocpi.platform.devices.platform_ad9361_spi_csts) on hdl container 0: PL:0, using platform_ad9361_spi_csts/c/platform_ad9361_spi_csts in ../../hdl/assemblies/fsk_modem_csts/container-fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts/target-zynq_ultra/fsk_modem_csts_z3u_base_cnt_1rx_1tx_mode_2_cmos_csts.bitz dated Wed Jan 12 16:18:16 2022
	 Application XML parsed and deployments (containers and artifacts) chosen [19 s 687 ms]
	 Application established: containers, workers, connections all created [0 s 173 ms]
	 ad9361_init : AD936x Rev 2 successfully initialized
	 Application started/running [0 s 754 ms]
	 Waiting for up to 15 seconds for application to finish
	 real_digitizer: sync pattern 0xFACE found
	 Application is now considered finished after waiting 15 seconds [15 s 6 ms]

#. Verification

   - Returning to the Development Host and viewing the output data file

     ``$ eog applications/fsk_dig_radio_ctrlr_csts/odata/odata.bin``
