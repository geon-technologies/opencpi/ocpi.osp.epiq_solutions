#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      AP
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
# Description: Describes SourceFiles that make calls upon when building the
#              zynq_ultra_z3u primitive
#==============================================================================

# This Makefile is for the primitive library: zynq_ultra_z3u

# Set this variable to any other primitive libraries that this library depends on.
# If they are remote from this project, use slashes in the name (relative or absolute)
# If they are in this project, they must be compiled first, and this requires that the
# PrimitiveLibraries variable be set in the hdl/primitives/Makefile such that the
# libraries are in dependency order.
#Libraries=

# Set this variable to the list of source files in dependency order
# If it is not set, all .vhd and .v files will be compiled in wildcard/random order,
# except that any *_pkg.vhd files will be compiled first
#SourceFiles=

# Remember two rules for OpenCPI primitive libraries, in order to be usable with all tools:
# 1. Any entity (VHDL) or module (verilog) must have a VHDL component declaration in zynq_ultra_z3u_pkg.vhd
# 2. Entities or modules to be used from outside the library must have the file name
#    be the same as the entity/module name, and one entity/module per file.

Libraries=fixed_float ocpi axi sdp platform

SourceFiles= \
        zynq_ultra_z3u_pkg.vhd \
        zynq_ultra_z3u_ps.vhd \
	zynq_system_zynq_ultra_ps_e_0_0/hdl/zynq_ultra_ps_e_v3_2_2.v \
	zynq_system_zynq_ultra_ps_e_0_0/synth/zynq_system_zynq_ultra_ps_e_0_0.v \
	zynq_system_zynq_ultra_ps_e_0_0/zynq_system_zynq_ultra_ps_e_0_0.xci

OnlyTargets=zynq_ultra

include $(OCPI_CDK_DIR)/include/hdl/hdl-library.mk
