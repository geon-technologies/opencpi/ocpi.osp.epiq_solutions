.. sit5356 RCC worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. Company:     Geon Technologies, LLC
   Author:      Joel Palmer
   Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
                Dissemination of this information or reproduction of this
                material is strictly prohibited unless prior written
                permission is obtained from Geon Technologies, LLC

:orphan:

.. _sit5356-RCC-worker:

``sit5356`` RCC worker
======================

Implementation of the sit5356 RCC worker for the SiTime sit5356 Super-TCXO.

Detail
------

The equation for the Control Word Value can be found in the sit5356-datasheet and should be
reviewed when determining input values (dig_pull_range_ctrl, ppm_shift_from_nominal) for a desired
(clock) frequency shift (shifted_frequency).

The sit5356_proxy worker allows the user to input the Digital Pull Range (dig_pull_range_ctrl)
and PPM Shift From Nominal (ppm_shift_from_nominal) values in order to obtain a desired
clock frequency shift. The sit5356 proxy worker provides the user the output of the Control Word
Value (ctrl_word_value) and Shifted Frequency (shifted_frequency) values so that the user can check
these outputs against their desired Control Word Value, and desired Shifted Frequency value.

References:

 * https://www.sitime.com/support/resource-library/datasheets/sit5356-datasheet

Issue
-----

OpenCPI Ticket: #2663

When reading a property that uses a ushort (16-bit) register will read the property byte-swapped.
This causes confusion when validating the data of a ushort property. To overcome this issue, printf
statements have been included in the sit5356_proxy.cc so that property values represent data that
accounts for this byte-swap.
