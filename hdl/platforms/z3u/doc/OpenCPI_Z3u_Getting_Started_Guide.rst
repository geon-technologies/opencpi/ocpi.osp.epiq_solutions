.. OpenCPI_Z3u_Getting_Started_Guide

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. Company:     Geon Technologies, LLC
   Author:      Joel Palmer
   Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
                Dissemination of this information or reproduction of this
                material is strictly prohibited unless prior written
                permission is obtained from Geon Technologies, LLC

.. _gsg-OpenCPI_Z3u_Getting_Started_Guide:

OpenCPI Z3u Getting Started Guide
=================================

This document provides installation information that is specific to the Epiq Solutions
Matchstiq Z3u OSP, herein ``Z3u``. It is assumed that the user understands the material
and procedures that are defined in both Epiq Solutions Matchstiq Z3u and OpenCPI
documentation. To avoid reproducing redundant information in an effort of keeping this
guide concise, the following documents are referenced to the tasks described in this
document:

**Epiq Solutions Documentation:**

The following documents are provided by *Epiq Solutions*

* *Matchstiq Z3u Hardware User Manual v1.3*

* *Matchstiq Z3u Firmware Developer's Guide v1.0.0*

* *Matchstiq Z3u Getting Started Guide v1.0*

**OpenCPI Documentation:**

* `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/v2.2.0/docs/OpenCPI_Installation_Guide.pdf>`_

* `OpenCPI User Guide <https://opencpi.gitlab.io/releases/v2.2.0/docs/OpenCPI_User_Guide.pdf>`_

* `OpenCPI Glossary <https://opencpi.gitlab.io/releases/v2.2.0/docs/OpenCPI_Glossary.pdf>`_

**OpenCPI Z3u OSP Developers Guides:**

Located: ``ocpi.osp.epiq_solutions/hdl/platforms/z3u/doc/``

* *Guide for developing the Epiq Solutions Matchstiq Z3u OSP*

* *Guide for developing Device / Proxy Workers for an i2c bus*


.. _gsg-Revision-History-label:

Revision History
----------------

.. csv-table::
   :header: "Revision", "Description of Change", "Date"
   :widths: 20,20,20

   "v1.0", "Initial Release", "1/2022"

.. _gsg-Software-Prerequisites-label:

Software Prerequisites
----------------------

It is assumed that the tasks defined in the `Enabling OpenCPI Development for Embedded Systems`
section of the *OpenCPI Installation Guide* have been successfully performed. Support for the
Z3u system is located in the ``ocpi.osp.epiq_solutions`` project, targeting the ``z3u`` OpenCPI
HDL (FPGA) platform and the ``xilinx18_3_aarch64`` OpenCPI RCC (software) platform.

.. note::

   On the Matchstiq Z3u, the FSBL is stored in a QSPI that is programmed by the vendor and is set
   to 2018.3. At the time of this writing, reconfiguration of the QSPI for OpenCPI purposes is
   not supported. Therefore, all OpenCPI development for the Z3u is restricted to version 2018.3 of
   Xilinx Vivado/SDK and PetaLinux 2018.3 tools.

.. _gsg-OpenCPI-Framework-installation-label:

OpenCPI Framework installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**The insturctions within this guide assume that a fresh installation of the OpenCPI v2.2.0
framework has been installed.**

#. Clone the OpenCPI framework

   ``$ cd /home/user``

   ``$ git clone https://gitlab.com/opencpi/opencpi.git``

   ``$ cd opencpi``

   ``$ git checkout tags/v2.2.0``

#. Complete the steps in the :ref:`gsg-Bug-Fix-to-the-framework-label` section of the APPENDIX

#. Install the framework  (Duration 30 min)

   ``$ cd /home/user/opencpi/``

   ``$ ./scripts/install-opencpi.sh``

#. Install for the zcu104 HDL Platform (Duration < 14 hours)

   .. note::

      This step is a workaround for an issue in the framework, where the framework does not
      properly install/build an HDL Platform that is located in the opencpi/projects/osps/.
      Therefore, the user is instructed to install for a built-in HDL platform that targets the
      same Xilinx device family as the ``z3u``. This will ensure that all of the HDL assets are
      built for the correct target device family (zynq_ultra).

   ..

   ``$ ocpiadmin install platform zcu104``

.. _gsg-RCC-Platforms-label:

RCC Platforms
^^^^^^^^^^^^^

The ``z3u`` HDL Platform requires the use of the RCC platform ``xilinx18_3_aarch64``. Since the
``xilinx18_3_aarch64`` RCC Platform is not implemented in this version (v2.2.0) of OpenCPI,
the user must create the RCC Platform for building the OpenCPI run-time utilities against
``xilinx18_3_aarch64``.

.. note::

   The steps described below are located in the *OpenCPI Z3u OSP Developers Guide*
   under the section: *Create new RCC Platform*.

#. Download and install Xilinx Vivado/SDK and PetaLinux 2018.3 tools

#. Complete the steps in the :ref:`gsg-Create-new-RCC-Platform-label` section of the APPENDIX

#. Install the rcc platform ``xilinx18_3_aarch64``

   ``$ cd /home/user/opencpi/``

   ``$ ocpiadmin install platform xilinx18_3_aarch64``

.. _gsg-Setup-and-Build-summary-label:

Setup and Build summary
^^^^^^^^^^^^^^^^^^^^^^^

Below is an abbreviated set of steps that guide the user through the installation and
deployment of the ``z3u`` HDL Platform with the ``xilinx18_3_aarch64`` RCC platform (created
in the :ref:`gsg-RCC-Platforms-label` section). This results in a single test executable application
named ``testbias`` based on the ``testbias`` HDL assembly (FPGA bitstream), which are both in the
``ocpi.assets`` built-in project.

The steps provided below rely heavily on the *OpenCPI Installation Guide*. For additional
information, reference the *OpenCPI Z3u OSP Developers Guide*

.. note::

   The values of the ”Duration” in the steps below are approximated and directly dependent on the
   system resources of the development host. They are roughly based on a development host having an
   Intel Xeon 3.90GHz x 16 with 128GB RAM.

#. Clone ocpi.osp.epiq_solutions project into the appropriate directory

   ``$ cd /home/user/opencpi/projects/osps``

   ``$ git clone ocpi.osp.epiq_solutions.git``

#. Register the ocpi.osp.epiq_solutions project

   ``$ cd ocpi.osp.epiq_solutions``

   ``$ ocpidev register project``

#. Build for the z3u target platform (Duration < 40 min)

   ``$ ocpidev build --hdl-platform z3u --rcc-platform xilinx18_3_aarch64``

#. Install HDL platform (Duration < 20 min)

   ``$ cd /home/user/opencpi``

   ``$ ocpiadmin install platform z3u``

#. Deploy RCC platform onto the HDL platform

   ``$ ocpiadmin deploy platform xilinx18_3_aarch64 z3u``

.. _gsg-Hardware-Prerequisites-label:

Hardware Prerequisites
----------------------

The Z3u OSP was developed against hardware having the following version info:

.. csv-table::
   :widths: 20,20,20

   "libsidekiq", "v4.15.1", "Note: Not critical to the OSP"
   "FPGA Firmware", "v3.14.1 (0x08fefa52)", "Note: Not critical to the OSP"
   "Device S/N", "9X1N", "rev C"

.. _gsg-Matchstiq-Z3u-PDK-Package-label:

Matchstiq Z3u PDK Package
^^^^^^^^^^^^^^^^^^^^^^^^^

The Z3u PDK product package includes the following items:

* One Matchstiq Z3u unit

* One un-housed Matchstiq Z3u unit connected to a debug-breakout board, for access to PL+PS GPIO,
  FPGA, JTAG, and processor UART

* Access to libsidekiq software API/library to allow custom application development

* Source code to test application demonstrating use of libsidekiq software API/library

* Source code license to the Zynq UltraScale+ Z3u reference design to allow custom FPGA development

* GPS antenna

The following items must be provided by the user:

* MicroSD card

* USB-A to USB-C Connector (Power / ETHERNET)

* USB-A to Micro-USB (UART)

.. _gsg-Matchstiq-Z3u-Device-Overview-label:

Matchstiq Z3u Device Overview
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

**Micro-USB**

The micro-USB serial port, labeled uUSB in the picture below, can be used to access the serial
connection of the processor.

**MicroSD Card**

The MicroSD Card labeled uSD in the picture below is used to load the microSD card contents of the
System Image boot artifacts.

**Ethernet Cable**

The USB3 port in the picture below is used as both an Ethernet connection from the development host
to the device and as a source of power to the device.

**DHCP Support**

The Matchstiq Z3u is a master Ethernet device (192.168.0.15) and automatically configures the
connected Host with the appropriate Ethernet interface having address 192.168.0.20.


.. figure:: images/z3u_device_overview.jpg
   :alt: Z3u Device Overview
   :align: center

.. _gsg-Populating-artifacts-onto-the-microSD-card-label:

Populating artifacts onto the microSD card
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once the ``z3u`` HDL Platform and ``xilinx18_3_aarch64`` RCC Platform have been successfully
installed and deployed in the :ref:`gsg-Setup-and-Build-summary-label`, the following steps can be
taken in order to create a valid microSD card to boot the Z3u system.

#. Complete the steps in the :ref:`gsg-Creating a valid SD-Card-label` sections of the APPENDIX

#. With a properly configure microSD card having ``boot/`` and ``root/`` partitions

   #. ``$ cd /home/user/opencpi/cdk/z3u/sdcard-xilinx18_3_aarch64``

   #. ``$ cp image.ub /run/media/<user>/boot/``

   #. ``$ sudo tar xvf rootfs.tar.gz -C /run/media/<user>/root/``

   #. ``$ sudo cp -RLp opencpi/ /run/media/<user>/root/home/root/``

   #. ``$ umount /dev/sda1`` and ``umount /dev/sda2``

   #. Remove the microSD card from Host

.. _gsg-Setting-up-the-Hardware-label:

Setting up the Hardware
-----------------------

The *OpenCPI Installation Guide* describes the procedure to establish a serial console and boot the
system. This section provides Z3u specific information that applies to these tasks.

.. _gsg-Establish-a-Serial-Console-Connection-label:

Establish a Serial Console Connection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The *OpenCPI Installation Guide* has a section ``Configuring the Runtime Environment on the Embedded
System`` that describes the procedure to establish a serial console. The Z3u console serial port is
configured for 115200 baud.

The cable used is a micro-USB to USB-A cable to connect its console micro-USB UART port to the
development host.

.. _gsg-Booting-the-Z3u-from-the-microSD-card-label:

Booting the Z3u from the microSD card
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The Z3u is setup to boot the default factory system image that exists in the local eMMC memory. In
order to bypass the default system image that is stored in the eMMC and instead load the OpenCPI
image artifacts that are stored on the microSD card, perform the following steps:

#. Remove power from the z3u unit

#. With the contents provided in the :ref:`gsg-Populating-artifacts-onto-the-microSD-card-label`
   section, insert the microSD card into the Z3u microSD card slot

#. Attach a serial cable (micro-USB to USB-A) from the Z3u to the host

#. Attach a USB-C cable from the Z3u to the host. This applies power to the Z3u and begins the
   boot process

#. Establish a serial connection

   ``$ sudo screen /dev/ttyUSB0 115200``

#. Enter into the U-boot setup. This may require rebooting the Z3u to catch the U-boot countdown
   and cancel the full boot sequence::
      ...
      DRAM:  2 GiB
      EL Level:       EL2
      Chip ID:        zu3eg
      MMC:   sdhci_transfer_data: Error detected in status(0x408000)!
      mmc@ff160000: 0 (SD), mmc@ff170000: 1 (eMMC)
      SF: Detected n25q1024a with page size 256 Bytes, erase size 64 KiB, total 128 MiB
      In:    serial@ff010000
      Out:   serial@ff010000
      Err:   serial@ff010000
      Board: Xilinx ZynqMP
      Hit any key to stop autoboot:  0
      ZynqMP>

#. Within the U-boot editor, set environment variables to temporarily boot from the microSD card::

      ZynqMP> setenv force_sdboot 1
      ZynqMP> setenv bootargs ${bootargs} root=/dev/mmcblk0p2 rootwait rw
      ZynqMP> boot

This will successfully boot the OpenCPI system image artifacts that are located on the microSD
card.

.. _gsg-Configuring-the-Runtime-Environment-on-the-Platform-label:

Configuring the Runtime Environment on the Platform
---------------------------------------------------

The *OpenCPI Installation Guide* provides the procedure for setting up and verifying the run-time
environment. This system is initially set with **“root”** for user name and password.

After a successful boot to PetaLinux, login to the system, using **“root“** for user name and
password.

Take note of the **root@z3u** indicating that the Control-Plane and Data-Plane of the Z3u have
successfully booted using PetaLinux.

Verify that the following login is observed (usr/pw = root/root)::

   ...
   PetaLinux 2018.3 z3u /dev/ttyPS0

   z3u login:

   root@z3u:~# uname -a
   Linux z3u 4.14.0-xilinx-v2018.3 #1 SMP Tue Dec 7 19:58:12 UTC 2021 aarch64 GNU/Linux

.. warning::

   If you are presented with the error in the "codeblock" after entering the Z3u login
   credentials, remove the "/home/root/.profile", reboot the Z3u and login.

   ::

      z3u login: root
      Password:
      Executing /home/root/.profile.
      -sh: /release: No such file or directory
      No reserved DMA memory found on the linux boot command line.
      [   27.507843] opencpi: loading out-of-tree module taints kernel.
      [   27.514712] opencpi: dma_set_coherent_mask failed for device ffffffc06ce74800
      [   27.521903] opencpi: get_dma_memory failed in opencpi_init, trying fallback
      [   27.528893] NET: Registered protocol family 12
      Driver loaded successfully.
      OpenCPI ready for zynq.
      Loading bitstream
      Exiting for problem: error loading device pl:0: Can't open bitstream file '/home/root/opencpi/artifacts/testbias__base.bitz' for reading: No such file or directory(2)
      Bitstream load error

   ..

   % ``rm /home/root/.profile``

   % ``reboot``

..

.. _gsg-Standalone-Mode-setup-label:

Standalone Mode setup
^^^^^^^^^^^^^^^^^^^^^

The goal of this section is to enable the user with the ability to setup the ``Standalone Mode``
on the Z3u. Success of this section is the ability to source the customized ``mysetup.sh``
script that enables the Standalone Mode and provides the ability to load bitstreams from the
microSD card to the Platform Host (Z3u).

#. Browse to the OpenCPI installation directory

   ``# cd /home/root/opencpi/``

#. Create the ``mysetup.sh`` for editing

   ``# cp default_mysetup.sh ./mysetup.sh``

#. Edit the ``mysetup.sh``. (NOT TYPICALLY REQUIRED)

#. Source the ``mysetup.sh`` script to enable OpenCPI Standalone Mode

   ``# cd opencpi/``

   ``# export OCPI_LOCAL_DIR=/home/root/opencpi``

   ``# source /home/root/opencpi/mysetup.sh``


.. _gsg-Network-Mode-setup-label:

Network Mode setup
^^^^^^^^^^^^^^^^^^

The goal of this section is to enable the user with the ability to setup the Network Mode on the
Z3u. Success of this section is the ability to source the customized ``mynetsetup.sh`` script that
enables the Network Mode and provides the ability to load bitstreams from the Development Host
(Computer) to the Platform Host (Z3u).

#. Configure the Development Host's ``/etc/exports.d/user.exports`` to allow the NFS mounting
   of the framework and it's projects, including the osp/ , for example. Restart the
   NFS server.

   /home/user/opencpi 192.168.0.0/16(rw,sync,no_root_squash,crossmnt,fsid=38)

#. Mount the OpenCPI filesystem

   ``# cd /home/root/opencpi/``

#. Create the ``mynetsetup.sh`` for editing

   ``# cp default_mynetsetup.sh ./mynetsetup.sh``

   ``# vi mynetsetup.sh``

#. To access the ``opencpi/`` project directory on the development host, configure the
   ``mynetsetup.sh`` to reflect the development host path. Under the section ``# Mount the opencpi
   development system as an NFS server, onto /mnt/net ...``, add the following lines which are
   necessary for mounting the FOSS build in projects core, assets, assets_ts, and platform, along
   with the Epiq Solutions Z3u OSP platform::

        mkdir -p /mnt/net
        mount -t nfs -o udp,nolock,soft,intr $1:$2 /mnt/net  # second argument should be location of opencpi directory
        mkdir -p /mnt/ocpi_core
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/core /mnt/ocpi_core
        mkdir -p /mnt/ocpi_assets
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets /mnt/ocpi_assets
        mkdir -p /mnt/ocpi_assets_ts
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/assets_ts /mnt/ocpi_assets_ts
        mkdir -p /mnt/ocpi_platform
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/platform /mnt/ocpi_platform
        mkdir -p /mnt/ocpi_z3u
        mount -t nfs -o udp,nolock,soft,intr $1:/home/user/opencpi/projects/osps/ocpi.osp.epiq_solutions /mnt/ocpi_z3u

#. Source the ``mynetsetup.sh`` script to enable OpenCPI Network Mode

   ``# cd opencpi/``

   ``# export OCPI_LOCAL_DIR=/home/root/opencpi``

   ``# source /home/root/opencpi/mynetsetup.sh <dev host IP address> /home/user/opencpi``

   .. note::

      Example: ``source /home/root/opencpi/mynetsetup.sh 192.168.0.20 /home/user/opencpi``

   ..

    ::

       root@/home/root/opencpi# source /home/root/opencpi/mynetsetup.sh 192.168.0.20 /home/user/opencpi
       An IP address was detected.
       My IP address is: 192.168.0.15, and my hostname is: z3u
       Attempting to set time from time.nist.gov
       rdate: bad address 'time.nist.gov'
       ====YOU HAVE NO NETWORK CONNECTION and NO HARDWARE CLOCK====
       Set the time using the "date YYYY.MM.DD-HH:MM[:SS]" command.
       Running login script.
       OCPI_CDK_DIR is now /mnt/net/cdk.
       OCPI_ROOT_DIR is now /mnt/net.
       Executing /home/root/.profile
       No reserved DMA memory found on the linux boot command line.
       [  480.135392] opencpi: loading out-of-tree module taints kernel.
       [  480.142325] opencpi: dma_set_coherent_mask failed for device ffffffc06cebcc00
       [  480.149535] opencpi: get_dma_memory failed in opencpi_init, trying fallback
       [  480.156544] NET: Registered protocol family 12
       Driver loaded successfully.
       OpenCPI ready for zynq.
       Loading bitstream
       Bitstream successfully loaded
       Discovering available containers...
       Available containers:
       #  Model Platform            OS     OS-Version  Arch     Name
       0  hdl   z3u                                             PL:0
       1  rcc   xilinx18_3_aarch64  linux  18_3        aarch64  rcc0
       %

    ::

       % pwd
       /home/root/opencpi
       %
       % env | grep OCPI
       OCPI_LOCAL_DIR=/home/root/opencpi
       OCPI_ROOT_DIR=/mnt/net
       OCPI_HDL_PLATFORM=
       OCPI_LIBRARY_PATH=/mnt/net/project-registry/ocpi.core/exports/artifacts:/mnt/net/cdk/xilinx18_3_aarch64/artifacts:/mnt/net/projects/assets/artifacts:/home/root/opencpi/xilinx18_3_aarch64/artifacts
       OCPI_TOOL_PLATFORM=xilinx18_3_aarch64
       OCPI_RELEASE=opencpi-v2.1.0
       OCPI_CDK_DIR=/mnt/net/cdk
       OCPI_TOOL_DIR=xilinx18_3_aarch64
       OCPI_SYSTEM_CONFIG=/home/root/opencpi/system.xml
       OCPI_DEFAULT_HDL_DEVICE=pl:0
       OCPI_ENABLE_HDL_SIMULATOR_DISCOVERY=0
       OCPI_TOOL_OS=linux
       %

.. _gsg-Run-an-Application-label:

Run an Application
------------------

With :ref:`gsg-Network-Mode-setup-label` now enabled, the following steps run the test application
**testbias**, based on the testbias HDL assembly, both of which are from the assets built-in
project.

Instructions for running the testbias application in Standalone, Network or Server Mode are provided
below.

#. Setup runtime environment

   ``% export OCPI_DMA_CACHE_MODE=0`` (required if FOSS version is <v2.2.0)

.. _gsg-Run-app-in-Standalone-Mode-label:

Run app in Standalone Mode
^^^^^^^^^^^^^^^^^^^^^^^^^^

#. Browse to the ``testbias`` applications directory

   ``% cd /home/root/opencpi/applications``

#. Configure the OpenCPI artifacts search path:

   ``% export OCPI_LIBRARY_PATH=../artifacts:../xilinx18_3_aarch64/artifacts``

#. Confirm that the ``testbias`` application functions as expected by verifying the input and
   output are equal when assigning a testbias of zero 0 (no change).

   ``% ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml``

   stdout of screen session (Standalone Mode)::

     % cd /home/root/opencpi/applications/
     % export OCPI_LIBRARY_PATH=../artifacts/:../xilinx18_3_aarch64/artifacts/
     % export OCPI_DMA_CACHE_MODE=0
     % ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0^C
     % ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml
     Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
     Actual deployment is:
     Instance  0 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in ../xilinx18_3_aarch64/artifacts//ocpi.core.file_read.rcc.0.xilinx18_3_aarch64.so dated Mon Dec 13 19:04:08 2021
     Instance  1 bias (spec ocpi.core.bias) on hdl container 0: PL:0, using bias_vhdl/a/bias_vhdl in ../artifacts//testbias_z3u_base.bitz dated Mon Dec 13 19:04:08 2021
     Instance  2 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in ../xilinx18_3_aarch64/artifacts//ocpi.core.file_write.rcc.0.xilinx18_3_aarch64.so dated Mon Dec 13 19:04:08 2021
     Application XML parsed and deployments (containers and artifacts) chosen [0 s 40 ms]
     Application established: containers, workers, connections all created [0 s 66 ms]
     Dump of all initial property values:
     Property   0: file_read.fileName = "test.input" (cached)
     Property   1: file_read.messagesInFile = "false" (cached)
     Property   2: file_read.opcode = "0x0" (cached)
     Property   3: file_read.messageSize = "0x10"
     Property   4: file_read.granularity = "0x4" (cached)
     Property   5: file_read.repeat = "false"
     Property   6: file_read.bytesRead = "0x0"
     Property   7: file_read.messagesWritten = "0x0"
     Property   8: file_read.suppressEOF = "false"
     Property   9: file_read.badMessage = "false"
     Property  16: bias.biasValue = "0x0" (cached)
     Property  20: bias.test64 = "0x0"
     Property  31: file_write.fileName = "test.output" (cached)
     Property  32: file_write.messagesInFile = "false" (cached)
     Property  33: file_write.bytesWritten = "0x0"
     Property  34: file_write.messagesWritten = "0x0"
     Property  35: file_write.stopOnEOF = "true" (cached)
     Property  39: file_write.suppressWrites = "false"
     Property  40: file_write.countData = "false"
     Property  41: file_write.bytesPerSecond = "0x0"
     Application started/running [0 s 1 ms]
     Waiting for application to finish (no time limit)
     Application finished [0 s 20 ms]
     Dump of all final property values:
     Property   0: file_read.fileName = "test.input" (cached)
     Property   1: file_read.messagesInFile = "false" (cached)
     Property   2: file_read.opcode = "0x0" (cached)
     Property   3: file_read.messageSize = "0x10"
     Property   4: file_read.granularity = "0x4" (cached)
     Property   5: file_read.repeat = "false" (cached)
     Property   6: file_read.bytesRead = "0xfa0"
     Property   7: file_read.messagesWritten = "0xfa"
     Property   8: file_read.suppressEOF = "false" (cached)
     Property   9: file_read.badMessage = "false"
     Property  16: bias.biasValue = "0x0" (cached)
     Property  20: bias.test64 = "0x0" (cached)
     Property  31: file_write.fileName = "test.output" (cached)
     Property  32: file_write.messagesInFile = "false" (cached)
     Property  33: file_write.bytesWritten = "0xfa0"
     Property  34: file_write.messagesWritten = "0xfb"
     Property  35: file_write.stopOnEOF = "true" (cached)
     Property  39: file_write.suppressWrites = "false" (cached)
     Property  40: file_write.countData = "false" (cached)
     Property  41: file_write.bytesPerSecond = "0x44229"

#. Verify that the data has successfully transferred through the application by performing an
   m5sum on the input and output data files with bias effectively disabled, by setting the
   biasValue=0.

   - Compare the md5sum of both ``test.input`` and ``test.output``. The stdout should be as follows::

      % md5sum test.*
      2934e1a7ae11b11b88c9b0e520efd978  test.input
      2934e1a7ae11b11b88c9b0e520efd978  test.output

.. note::

   **This shows that with a biasvalue=0 (no change in data) that the input matches the output
   and the testbias application is working as it should.**
..

.. _gsg-Run-app-in-Network-Mode-label:

Run app in Network Mode
^^^^^^^^^^^^^^^^^^^^^^^

#. Browse to the ``testbias`` applications directory

   ``% cd /mnt/ocpi_assets/applications``

#. Configure the OpenCPI artifacts search path:

   ``% export OCPI_LIBRARY_PATH=/mnt/ocpi_assets/artifacts:/mnt/ocpi_core/artifacts``

#. Run the ``testbias`` application, setting the biasvalue to 0 (i.e. no change to input data)

   ``% ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml``

   stdout of screen session (Network Mode)::

     % ocpirun -v -d -x -m bias=hdl -p bias=biasvalue=0 testbias.xml
     Available containers are:  0: PL:0 [model: hdl os:  platform: z3u], 1: rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64]
     Actual deployment is:
     Instance  0 file_read (spec ocpi.core.file_read) on rcc container 1: rcc0, using file_read in /mnt/ocpi_core/artifacts/ocpi.core.file_read.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:08:09 2021
     Instance  1 bias (spec ocpi.core.bias) on hdl container 0: PL:0, using bias_vhdl/a/bias_vhdl in /mnt/ocpi_assets/artifacts/ocpi.assets.testbias_z3u_base.hdl.0.z3u.bitz dated Thu Jan 13 18:09:06 2022
     Instance  2 file_write (spec ocpi.core.file_write) on rcc container 1: rcc0, using file_write in /mnt/ocpi_core/artifacts/ocpi.core.file_write.rcc.0.xilinx18_3_aarch64.so dated Tue Nov 30 23:08:36 2021
     Application XML parsed and deployments (containers and artifacts) chosen [3 s 901 ms]
     Application established: containers, workers, connections all created [0 s 69 ms]
     Dump of all initial property values:
     Property   0: file_read.fileName = "test.input" (cached)
     Property   1: file_read.messagesInFile = "false" (cached)
     Property   2: file_read.opcode = "0x0" (cached)
     Property   3: file_read.messageSize = "0x10"
     Property   4: file_read.granularity = "0x4" (cached)
     Property   5: file_read.repeat = "false"
     Property   6: file_read.bytesRead = "0x0"
     Property   7: file_read.messagesWritten = "0x0"
     Property   8: file_read.suppressEOF = "false"
     Property   9: file_read.badMessage = "false"
     Property  16: bias.biasValue = "0x0" (cached)
     Property  20: bias.test64 = "0x0"
     Property  31: file_write.fileName = "test.output" (cached)
     Property  32: file_write.messagesInFile = "false" (cached)
     Property  33: file_write.bytesWritten = "0x0"
     Property  34: file_write.messagesWritten = "0x0"
     Property  35: file_write.stopOnEOF = "true" (cached)
     Property  39: file_write.suppressWrites = "false"
     Property  40: file_write.countData = "false"
     Property  41: file_write.bytesPerSecond = "0x0"
     Application started/running [0 s 16 ms]
     Waiting for application to finish (no time limit)
     Application finished [0 s 20 ms]
     Dump of all final property values:
     Property   0: file_read.fileName = "test.input" (cached)
     Property   1: file_read.messagesInFile = "false" (cached)
     Property   2: file_read.opcode = "0x0" (cached)
     Property   3: file_read.messageSize = "0x10"
     Property   4: file_read.granularity = "0x4" (cached)
     Property   5: file_read.repeat = "false" (cached)
     Property   6: file_read.bytesRead = "0xfa0"
     Property   7: file_read.messagesWritten = "0xfa"
     Property   8: file_read.suppressEOF = "false" (cached)
     Property   9: file_read.badMessage = "false"
     Property  16: bias.biasValue = "0x0" (cached)
     Property  20: bias.test64 = "0x0" (cached)
     Property  31: file_write.fileName = "test.output" (cached)
     Property  32: file_write.messagesInFile = "false" (cached)
     Property  33: file_write.bytesWritten = "0xfa0"
     Property  34: file_write.messagesWritten = "0xfb"
     Property  35: file_write.stopOnEOF = "true" (cached)
     Property  39: file_write.suppressWrites = "false" (cached)
     Property  40: file_write.countData = "false" (cached)
     Property  41: file_write.bytesPerSecond = "0x497a9"

#. Verify that the data has successfully transferred through the application by performing an
   m5sum on the input and output data files with bias effectively disabled, by setting the
   biasValue=0.

   - Compare the md5sum of both ``test.input`` and ``test.output``. The stdout should be as follows::

      % md5sum test.*
      2934e1a7ae11b11b88c9b0e520efd978  test.input
      2934e1a7ae11b11b88c9b0e520efd978  test.output

.. note::

   **This shows that with a biasvalue=0 (no change in data) that the input matches the output
   and the testbias application is working as it should.**
..

.. _gsg-Run-app-in-Server-Mode-label:

Run app in Server Mode
^^^^^^^^^^^^^^^^^^^^^^

Reference the OpenCPI documentation for details about Server Mode. Notice that the microSD
card does not require that the ``opencpi/`` be manually copied onto the microSD. In other words,
the microSD card only requires boot/ and root/ partitions, with the boot.bin copied into the
boot/, rootfs.tar.gz extracted into the root/ partition.

#. Install the microSD card into the Z3u and boot by following the instructions in section
:ref:`gsg-Booting-the-Z3u-from-the-microSD-card-label`

#. On the Host, setup a terminal for OpenCPI and ``ocpiremote`` access to Z3u

   ``$ cd /home/user/opencpi/
   ``$ . ./cdk/opencpi-setup.sh -s``
   ``$ export OCPI_SERVER_ADDRESSES=192.168.0.15:12345``
   ``$ export OCPI_SOCKET_INTERFACE=enp0s20f0u4c2``

#. Configure the OpenCPI artifacts search path:

   ``$ ocpiremote load -s xilinx18_3_aarch64 -w z3u``
   ``$ ocpiremote start -b``

#. Browse to the ``testbias`` applications directory

   ``$ cd /home/user/opencpi/projects/assets/applications``

#. Configure the OpenCPI artifacts search path:

   ``$ export OCPI_LIBRARY_PATH=../artifacts:../imports/ocpi.core/artifacts:../imports/ocpi.platform/artifacts``

#. Confirm that the ``testbias`` application functions as expected by verifying the input and
   output are equal when assigning a testbias of zero 0 (no change).

   % ``ocpirun -v -d -x -mbias=hdl -pbias=biasvalue=0 -Pfile_read=centos7 -Pfile_write=centos7 testbias.xml``

   stdout of screen session (Server Mode)::

     applications (release-2.2.0)$ ocpirun -v -d -x -mbias=hdl -pbias=biasvalue=0 -Pfile_read=centos7 -Pfile_write=centos7 testbias
     Received server information from "192.168.0.15:12345".  Available containers are:
     192.168.0.15:12345/PL:0              platform z3u, model hdl, os , version , arch , build
     Transports: ocpi-dma-pio,00:51:49:4d:79:e9,0,0,0x41,0x101|ocpi-socket-rdma, ,1,0,0x42,0x41|
     192.168.0.15:12345/rcc0              platform xilinx18_3_aarch64, model rcc, os linux, version 18_3, arch aarch64, build
     Transports: ocpi-dma-pio,00:51:49:4d:79:e9,1,0,0x103,0x103|ocpi-smb-pio,00:51:49:4d:79:e9,0,0,0xb,0xb|ocpi-socket-rdma, ,1,0,0x42,0x43|
     Available containers are:  0: 192.168.0.15:12345/PL:0 [model: hdl os:  platform: z3u], 1: 192.168.0.15:12345/rcc0 [model: rcc os: linux platform: xilinx18_3_aarch64], 2: rcc0 [model: rcc os: linux platform: centos7]
     Actual deployment is:
     Instance  0 file_read (spec ocpi.core.file_read) on rcc container 2: rcc0, using file_read in ../imports/ocpi.core/artifacts/ocpi.core.file_read.rcc.0.centos7.so dated Fri Jan 14 11:37:12 2022
     Instance  1 bias (spec ocpi.core.bias) on hdl container 0: 192.168.0.15:12345/PL:0, using bias_vhdl/a/bias_vhdl in ../artifacts/ocpi.assets.testbias_z3u_base.hdl.0.z3u.bitz dated Tue Jan 18 08:29:46 2022
     Instance  2 file_write (spec ocpi.core.file_write) on rcc container 2: rcc0, using file_write in ../imports/ocpi.core/artifacts/ocpi.core.file_write.rcc.0.centos7.so dated Fri Jan 14 11:37:36 2022
     Application XML parsed and deployments (containers and artifacts) chosen [0 s 210 ms]
     Application established: containers, workers, connections all created [0 s 176 ms]
     Dump of all initial property values:
     Property   0: file_read.fileName = "test.input" (cached)
     Property   1: file_read.messagesInFile = "false" (cached)
     Property   2: file_read.opcode = "0x0" (cached)
     Property   3: file_read.messageSize = "0x10"
     Property   4: file_read.granularity = "0x4" (cached)
     Property   5: file_read.repeat = "false"
     Property   6: file_read.bytesRead = "0x0"
     Property   7: file_read.messagesWritten = "0x0"
     Property   8: file_read.suppressEOF = "false"
     Property   9: file_read.badMessage = "false"
     Property  16: bias.biasValue = "0x0"
     Property  20: bias.test64 = "0x0"
     Property  31: file_write.fileName = "test.output" (cached)
     Property  32: file_write.messagesInFile = "false" (cached)
     Property  33: file_write.bytesWritten = "0x0"
     Property  34: file_write.messagesWritten = "0x0"
     Property  35: file_write.stopOnEOF = "true" (cached)
     Property  39: file_write.suppressWrites = "false"
     Property  40: file_write.countData = "false"
     Property  41: file_write.bytesPerSecond = "0x0"
     Application started/running [0 s 67 ms]
     Waiting for application to finish (no time limit)
     Application finished [0 s 40 ms]
     Dump of all final property values:
     Property   0: file_read.fileName = "test.input" (cached)
     Property   1: file_read.messagesInFile = "false" (cached)
     Property   2: file_read.opcode = "0x0" (cached)
     Property   3: file_read.messageSize = "0x10"
     Property   4: file_read.granularity = "0x4" (cached)
     Property   5: file_read.repeat = "false" (cached)
     Property   6: file_read.bytesRead = "0xfa0"
     Property   7: file_read.messagesWritten = "0xfa"
     Property   8: file_read.suppressEOF = "false" (cached)
     Property   9: file_read.badMessage = "false"
     Property  16: bias.biasValue = "0x0"
     Property  20: bias.test64 = "0x0"
     Property  31: file_write.fileName = "test.output" (cached)
     Property  32: file_write.messagesInFile = "false" (cached)
     Property  33: file_write.bytesWritten = "0xfa0"
     Property  34: file_write.messagesWritten = "0xfb"
     Property  35: file_write.stopOnEOF = "true" (cached)
     Property  39: file_write.suppressWrites = "false" (cached)
     Property  40: file_write.countData = "false" (cached)
     Property  41: file_write.bytesPerSecond = "0x20387"

#. Verify that the data has successfully transferred through the application by performing an
   m5sum on the input and output data files with bias effectively disabled, by setting the
   biasValue=0.

   - Compare the md5sum of both ``test.input`` and ``test.output``. The stdout should be as follows::

      % md5sum test.*
      2934e1a7ae11b11b88c9b0e520efd978  test.input
      2934e1a7ae11b11b88c9b0e520efd978  test.output

.. note::

   **This shows that with a biasvalue=0 (no change in data) that the input matches the output
   and the testbias application is working as it should.**
..

.. _gsg-GPS-Reveiver-label:

GPS Receiver
------------

Reference the *Matchstiq Z3u Getting Started Guide v1.0* for details of the GPS not found here.

GPS Fix LED:

For the initial release of the OSP, the LED driven by ``FIX_LED`` of the GPS receiver which cycles
0.5 sec ON and 4 sec OFF when the GPS is in the ``fixed`` state. Future releases drive the LED
constant while the GPS receiver is in a ``fixed`` state.

.. _gsg-OpenCPI-control-of-GPS-receiver-label:

OpenCPI control of GPS receiver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The GPS receiver's UART interface is not supported by the initial release of the Z3u OSP.

Control of the GPS receiver's power and reset, and control of the GPS antenna biasing circuit are
accessable via the Zynq UltraScale+ Processing System.

- PS_MIO28: GPS_RESET_N
- PS_MIO29: GPS_POWER_EN
- PS_MIO32: GPS_ANT_BIAS_EN

A set of scripts at hdl/platforms/z3u/scripts/gps_power_on_off/ are provided for controlling these
signals. Review the README for details of each script.

Two Platform Worker properties ``pps_src_ext`` and ``using_pps`` configure the usage of the GPS's
1 PPS for Time Server Worker.

- ``pps_src_ext``: selects the 1 PPS source of the Time Server Worker
  - true  = GPS receiver's 1 PPS output (DEFAULT)
  - false = ``PPS`` external connector

- ``using_pps``: selects if Time Server Worker is synchronizing to an external PPS or free-running
  - true  = enabled
  - false = disabled (DEFAULT)

.. _gsg-Quick-test-of-GPS-label:

Quick test of GPS
^^^^^^^^^^^^^^^^^

GOAL:

Use the GPS LED and the property status of the Time Server Worker to validate the state of the
GPS receiver.

#. Attach an GPS antenna to the Z3u and position the antenna near a window

#. Power on the Z3u and set it up for OpenCPI

#. Run an app to load a bitstream or manually load a bitstream

   #. If manually loading a bitstream, the time_server.hdl must be manually taken out of reset.

      - ``% ocpihdl wunreset 1``

#. Check the property status of the time_server.hdl. Notice that the ``PPS_ok`` and ``time_valid``
   are ``false``.

.. code-block::

   % ocpihdl get -v 1
   Instance p/time_server of io worker time_server (spec ocpi.core.devices.time_server) with index 1
   0           ocpi_debug: false
   1          ocpi_endian: little
   2         ocpi_version: 0
   3             time_now: 401064522256028
   4                delta: 0
   5               PPS_ok: false
   6 enable_time_now_updates_from_PPS: <unreadable>
   7 valid_requires_write_to_time_now: <unreadable>
   8            frequency: 100000000
   9    PPS_tolerance_PPM: 1000
   10             PPS_test: false
   11 clr_status_sticky_bits: <unreadable>
   12 force_time_now_to_free_running: <unreadable>
   13       PPS_out_source: <unreadable>
   14 force_time_now_valid: <unreadable>
   15 force_time_now_invalid: <unreadable>
   16 PPS_lost_sticky_error: true
   17 time_now_updated_by_PPS_sticky: true
   18  time_now_set_sticky: true
   19 PPS_lost_last_second_error: false
   20            PPS_count: 116
   21     ticks_per_second: 99998710
   22            using_PPS: true
   23           time_valid: false

#. Run script to enable the antenna bias circuit, apply power to the GPS receiver and disable
   GPS receiver reset

   #. cd hdl/platforms/z3u/scripts/gps_power_on_off/
   #. ./power_on_gps.sh

#. After a few moments, the GPS LEDs should begin to blink. Recheck the task status of
   the time_server.hdl. Now notice that the ``PPS_ok`` and ``time_valid`` are now ``true``.

.. code-block::

   % ocpihdl get -v 1
   Instance p/time_server of io worker time_server (spec ocpi.core.devices.time_server) with index 1
   0           ocpi_debug: false
   1          ocpi_endian: little
   2         ocpi_version: 0
   3             time_now: 401928007698373
   4                delta: 0
   5               PPS_ok: true
   6 enable_time_now_updates_from_PPS: <unreadable>
   7 valid_requires_write_to_time_now: <unreadable>
   8            frequency: 100000000
   9    PPS_tolerance_PPM: 1000
   10             PPS_test: false
   11 clr_status_sticky_bits: <unreadable>
   12 force_time_now_to_free_running: <unreadable>
   13       PPS_out_source: <unreadable>
   14 force_time_now_valid: <unreadable>
   15 force_time_now_invalid: <unreadable>
   16 PPS_lost_sticky_error: true
   17 time_now_updated_by_PPS_sticky: true
   18  time_now_set_sticky: true
   19 PPS_lost_last_second_error: false
   20            PPS_count: 136
   21     ticks_per_second: 99998727
   22            using_PPS: true
   23           time_valid: true


.. _gsg-APPENDIX-label:

APPENDIX
--------

.. _gsg-Bug-Fix-to-the-framework-label:

Bug Fixes to the framework
^^^^^^^^^^^^^^^^^^^^^^^^^^

In support of building and installing the Z3u, the following bug fixes are required **PRIOR** to
building the Matchstiq Z3u HDL Platform Worker and FSK assembly, respectively.

#.

   ::

      $ git diff /home/user/opencpi/tools/include/hdl/hdl-targets.mk
      WARNING: terminal is not fully functional
      -  (press RETURN)
      diff --git a/tools/include/hdl/hdl-targets.mk b/tools/include/hdl/hdl-targets.mk
      index 6815474..c2f7d9a 100644
      --- a/tools/include/hdl/hdl-targets.mk
      +++ b/tools/include/hdl/hdl-targets.mk
      @@ -83,7 +83,7 @@ HdlTargets_zynq_ise:=$(foreach tgt,$(HdlTargets_zynq),$(tgt)_ise_alias)
       # ev: quad core, mali gpu, H.264
       # eg: quad core, mali gpu
       # cg: dual core
      -HdlTargets_zynq_ultra:=xczu28dr xczu9eg xczu7ev xczu3cg
      +HdlTargets_zynq_ultra:=xczu28dr xczu9eg xczu7ev xczu3cg xczu3eg
       # Zynq UltraScale+ chips require full part to be specified
       # The default is based on the zcu104 dev board, which is the cheapest, and supported by webpack.
       HdlDefaultTarget_zynq_ultra:=xczu3cg-2-sbva484e

#.

   ::

      $ git diff /home/user/opencpi/projects/assets/components/dsp_comps/dc_offset_filter.hdl/dc_offset_filter.xml
      WARNING: terminal is not fully functional
      -  (press RETURN)
      diff --git a/projects/assets/components/dsp_comps/dc_offset_filter.hdl/dc_offset_filter.xml b/projects/assets/components/dsp_comps/index b872e84..4d644d0 100644
      --- a/projects/assets/components/dsp_comps/dc_offset_filter.hdl/dc_offset_filter.xml
      +++ b/projects/assets/components/dsp_comps/dc_offset_filter.hdl/dc_offset_filter.xml
      @@ -1,8 +1,8 @@
      -<HdlWorker Language="vhdl" Spec="dc_offset_filter-spec.xml" Version="2" DataWidth="32"
      -          ExactParts='zynq:xc7z020-1-clg484
      +<HdlWorker Language="vhdl" Spec="dc_offset_filter-spec.xml" Version="2" DataWidth="32">
      +<!--      ExactParts='zynq:xc7z020-1-clg484
                             zynq_ise:xc7z020_ise_alias-1-clg484
                             virtex6:xc6vlx240t-1-ff1156
      -                      stratix4:ep4sgx230k-c2-f40'>
      +                      stratix4:ep4sgx230k-c2-f40'-->

         <Property Name="LATENCY_p" Type="uchar" Parameter="true" Default="1"
                  Description='Number of clock cycles between a valid input and a valid output'/>

.. _gsg-Create-new-RCC-Platform-label:

Create new RCC Platform
^^^^^^^^^^^^^^^^^^^^^^^

Create new RCC Platform: ``core/rcc/platforms/xilinx18_3_aarch64`` to target the Z3u

#. ``$ cd /home/user/opencpi/projects/core/rcc/platforms/``

#. Copy the core RCC platform ``xilinx19_2_aarch64`` directory and rename it
   ``xilinx18_3_aarch64``

   ``$ cp -rf xilinx19_2_aarch64/ ./xilinx18_3_aarch64``

#. ``$ cd xilinx18_3_aarch64/``

#. Ensure that the newly created ``xilinx18_3_aarch64/`` is void of old artifacts,
   i.e. remove ``gen/`` and ``lib/`` that may have been copied from xilinx19_2_aarch64

#. Change file names in ``xilinx18_3_aarch64/`` from ``19_2`` to ``18_3``

#. Edit the contents of ``xilinx18_3_aarch64.mk`` per the below:

   ::

      #OcpiXilinxLinuxRepoTag:=xilinx-v2019.2.01

      include $(OCPI_CDK_DIR)/include/xilinx/xilinx-rcc-platform-definition.mk
      OcpiCXXFlags+=-fno-builtin-memset -fno-builtin-memcpy
      OcpiCFlags+=-fno-builtin-memset -fno-builtin-memcpy
      OcpiPlatformOs:=linux
      OcpiPlatformOsVersion:=18_3
      OcpiPlatformArch:=aarch64

#. Unregister and reregister the project:

   ``$ cd /home/user/opencpi/projects/core``

   ``$ ocpidev unregister project``

   ``$ ocpidev register project``

#. Verify that the xilinx18_3_aarch64 project has been registered:

   ``$ cd /home/user/opencpi``

   ``$ ocpidev show platforms``

.. _gsg-Creating a valid SD-Card-label:

Creating a valid SD-Card
""""""""""""""""""""""""

A valid SD-Card with ``/boot`` and ``/root`` partitions need to be made.

#. Be sure to save off any important information on the SD card

#. ``$ sudo umount /dev/sda1`` and/or ``sudo umount /dev/sda2``

#. ``$ sudo fdisk /dev/sda``

#. List the current partition table

   Command (m for help): ``p``

#. Remove all current partitions

   Command (m for help): ``d``

#. Make the following selections to create two partitions

   #. New ``n``, Primary ``p``, Partition number ``1``, First sector [enter] (default),
      Last sector size ``+1G``

   #. New ``n``, Primary ``p``, Partition number ``2``, First sector [enter] (default), Last sector
      size [enter] (default)

#. Write table to disk and exit

   Command (m for help): ``w``

#. Uninstall and reinstall the SD Card / USB drive

#. ``$ sudo umount /dev/sda1`` and/or ``sudo umount /dev/sda2``

#. ``$ sudo mkfs.vfat -F 32 -n boot /dev/sda1``

#. ``$ sudo mkfs.ext4 -L root /dev/sda2``

   ::

      mke2fs 1.42.9 (28-Dec-2013)
      Filesystem label=root
      OS type: Linux
      Block size=4096 (log=2)
      Fragment size=4096 (log=2)
      Stride=0 blocks, Stripe width=0 blocks
      907536 inodes, 3627136 blocks
      181356 blocks (5.00%) reserved for the super user
      First data block=0
      Maximum filesystem blocks=2151677952
      111 block groups
      32768 blocks per group, 32768 fragments per group
      8176 inodes per group
      Superblock backups stored on blocks:
              32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208

      Allocating group tables: done
      Writing inode tables: done
      Creating journal (32768 blocks): done
      Writing superblocks and filesystem accounting information: done

#. Uninstall and reinstall the microSD card

#. Check that both partitions (boot, root) have been created
