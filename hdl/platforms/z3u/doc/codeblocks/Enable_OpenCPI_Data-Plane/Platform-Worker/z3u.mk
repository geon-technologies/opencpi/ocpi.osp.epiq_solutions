#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      AP
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
# Description: Assigns the particular Vivado Board Part number and the RCC 
#              platform that it can target
#==============================================================================

HdlPart_z3u=xczu3eg-1-sbva484i
HdlRccPlatform_z3u=xilinx18_3_aarch64
