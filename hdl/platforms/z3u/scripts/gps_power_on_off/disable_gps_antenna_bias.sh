#!/bin/bash
#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      Jerry Darko
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
# Description: This is an HDL platform Makefile for the "z3u" platform
#==============================================================================

MIO_DIRECTION=$(devmem 0xFF0A0244)
MIO_OUTPUT_EN=$(devmem 0xFF0A0248)
if [[ "$MIO_DIRECTION" == "0x00000000" ]]; then
  devmem 0xFF0A0244 32 0x0000004C
fi

if [[ "$MIO_OUTPUT_EN" == "0x00000000" ]]; then
  devmem 0xFF0A0248 32 0x0000004C
fi

devmem 0xFF0A0008 32 0xFFBF0000
