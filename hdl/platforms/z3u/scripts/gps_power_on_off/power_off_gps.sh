#!/bin/bash
#==============================================================================
# Company:     Geon Technologies, LLC
# Author:      Jerry Darko
# Copyright:   (c) 2018 Geon Technologies, LLC. All rights reserved.
#              Dissemination of this information or reproduction of this
#              material is strictly prohibited unless prior written
#              permission is obtained from Geon Technologies, LLC
# Description: This is an HDL platform Makefile for the "z3u" platform
#==============================================================================

./disable_gps_antenna_bias.sh
./disable_gps_power.sh