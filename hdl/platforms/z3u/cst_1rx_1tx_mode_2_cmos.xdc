#
# From v3.15.0?? reference design package: sikekiq_z3u.xdc
#
#set_property direction INOUT [get_ports {gpio[0]}]
#set_property direction IN    [get_ports {data_clk}]
#set_property direction OUT   [get_ports {eeprom_wp}]
#set_property direction OUT   [get_ports {en_io_exp}]
#set_property direction OUT   [get_ports {fb_clk}]
#set_property direction IN    [get_ports {ref_40mhz}]
#set_property direction IN    [get_ports {gps_pps_in}]
#set_property direction OUT   [get_ports {uart0_tx}]
#set_property direction IN    [get_ports {uart0_rx}]
#set_property direction INOUT [get_ports {scl}]
#set_property direction INOUT [get_ports {sda}]
#set_property direction OUT   [get_ports {ref_osc_pwr_en}]
#set_property direction OUT   [get_ports {rfic_enable}]
#set_property direction OUT   [get_ports {rfic_en_agc}]
#set_property direction OUT   [get_ports {rfic_txnrx}]
#set_property direction OUT   [get_ports {rfic_ctrl_in[0]}]
#set_property direction OUT   [get_ports {rfic_ctrl_in[1]}]
#set_property direction OUT   [get_ports {rfic_ctrl_in[2]}]
#set_property direction OUT   [get_ports {rfic_ctrl_in[3]}]
#set_property direction IN    [get_ports {adc_dig_bus_a[0]}]
#set_property direction IN    [get_ports {adc_dig_bus_a[1]}]
#set_property direction IN    [get_ports {adc_dig_bus_a[2]}]
#set_property direction IN    [get_ports {adc_dig_bus_a[3]}]
#set_property direction IN    [get_ports {adc_dig_bus_a[4]}]
#set_property direction IN    [get_ports {adc_dig_bus_a[5]}]
#set_property direction IN    [get_ports {adc_dig_bus_a[6]}]
#set_property direction IN    [get_ports {adc_dig_bus_a[7]}]
#set_property direction OUT   [get_ports {rf_rst_n}]
#set_property direction OUT   [get_ports {spi_clk}]
#set_property direction OUT   [get_ports {spi_mosi}]
#set_property direction IN    [get_ports {spi_miso}]
#set_property direction OUT   [get_ports {spi_en_lo}]
#set_property direction OUT   [get_ports {multi_sync}]
#set_property direction INOUT [get_ports {scl_1}]
#set_property direction INOUT [get_ports {sda_1}]
#set_property direction IN    [get_ports {rx_data[0]}]
#set_property direction IN    [get_ports {rx_data[1]}]
#set_property direction IN    [get_ports {rx_data[2]}]
#set_property direction IN    [get_ports {rx_data[3]}]
#set_property direction IN    [get_ports {rx_data[4]}]
#set_property direction IN    [get_ports {rx_data[5]}]
#set_property direction IN    [get_ports {rx_data[6]}]
#set_property direction IN    [get_ports {rx_data[7]}]
#set_property direction IN    [get_ports {rx_data[8]}]
#set_property direction IN    [get_ports {rx_data[9]}]
#set_property direction IN    [get_ports {rx_data[10]}]
#set_property direction IN    [get_ports {rx_data[11]}]
#set_property direction IN    [get_ports {rx_frame}]
#set_property direction INOUT [get_ports {scl_2}]
#set_property direction INOUT [get_ports {sda_2}]
#set_property direction INOUT [get_ports {{gpio[1]}]
#set_property direction INOUT [get_ports {{gpio[2]}]
#set_property direction OUT   [get_ports {tx_data[0]}]
#set_property direction OUT   [get_ports {tx_data[1]}]
#set_property direction OUT   [get_ports {tx_data[2]}]
#set_property direction OUT   [get_ports {tx_data[3]}]
#set_property direction OUT   [get_ports {tx_data[4]}]
#set_property direction OUT   [get_ports {tx_data[5]}]
#set_property direction OUT   [get_ports {tx_data[6]}]
#set_property direction OUT   [get_ports {tx_data[7]}]
#set_property direction OUT   [get_ports {tx_data[8]}]
#set_property direction OUT   [get_ports {tx_data[9]}]
#set_property direction OUT   [get_ports {tx_data[10]}]
#set_property direction OUT   [get_ports {tx_data[11]}]
#set_property direction OUT   [get_ports {tx_frame}]
#set_property direction OUT   [get_ports {wfl_osc_en}]

#set_property BITSTREAM.CONFIG.PERSIST NO [current_design]

set_property IOSTANDARD  LVCMOS18 [get_ports gps_pps_in]
set_property PACKAGE_PIN P3       [get_ports gps_pps_in]
set_property IOSTANDARD  LVCMOS18 [get_ports pps_in]
set_property PACKAGE_PIN U2       [get_ports pps_in]
set_property IOSTANDARD  LVCMOS18 [get_ports gps_fix]
set_property PACKAGE_PIN P2       [get_ports gps_fix]
set_property IOSTANDARD  LVCMOS18 [get_ports gps_fix_led]
set_property PACKAGE_PIN U1       [get_ports gps_fix_led]

#set_property IOSTANDARD LVCMOS18 [get_ports ref_spi_dat]
#set_property PACKAGE_PIN T2      [get_ports ref_spi_dat]
#set_property IOSTANDARD LVCMOS18 [get_ports ref_spi_clk]
#set_property PACKAGE_PIN T3      [get_ports ref_spi_clk]

#set_property IOSTANDARD LVCMOS18  [get_ports ref_spi_pll_le]
#set_property PACKAGE_PIN R3       [get_ports ref_spi_pll_le]

#set_property SLEW SLOW            [get_ports gpio[1]]
##set_property PACKAGE_PIN T2      [get_ports TP8]
#set_property PACKAGE_PIN A8       [get_ports gpio[1]]
#set_property DRIVE 4              [get_ports gpio[1]]
#set_property IOSTANDARD LVCMOS18  [get_ports gpio[1]]
#set_property IOSTANDARD  LVCMOS18 [get_ports uart0_rx]
#set_property PACKAGE_PIN N2       [get_ports uart0_rx]
#set_property IOSTANDARD  LVCMOS18 [get_ports uart0_tx]
#set_property DRIVE       4        [get_ports uart0_tx]
#set_property SLEW        SLOW     [get_ports uart0_tx]
#set_property PACKAGE_PIN P1       [get_ports uart0_tx]

#set_property SLEW SLOW            [get_ports gpio[0]]
#set_property DRIVE 4              [get_ports gpio[0]]
#set_property IOSTANDARD LVCMOS18  [get_ports gpio[0]]
##set_property PACKAGE_PIN U1      [get_ports ACCEL_INT]
#set_property PACKAGE_PIN D6       [get_ports gpio[0]]
set_property IOSTANDARD  LVCMOS18 [get_ports eeprom_wp]
set_property DRIVE       4        [get_ports eeprom_wp]
set_property SLEW        SLOW     [get_ports eeprom_wp]
set_property PACKAGE_PIN T4       [get_ports eeprom_wp]
set_property IOSTANDARD  LVCMOS18 [get_ports scl_2]
set_property DRIVE       4        [get_ports scl_2]
set_property SLEW        SLOW     [get_ports scl_2]
set_property PACKAGE_PIN R1       [get_ports scl_2]
set_property IOSTANDARD  LVCMOS18 [get_ports sda_2]
set_property DRIVE       4        [get_ports sda_2]
set_property SLEW        SLOW     [get_ports sda_2]
set_property PACKAGE_PIN T1       [get_ports sda_2]
set_property IOSTANDARD LVCMOS18  [get_ports en_io_exp]
set_property DRIVE 4              [get_ports en_io_exp]
set_property SLEW SLOW            [get_ports en_io_exp]
set_property PACKAGE_PIN N3       [get_ports en_io_exp]

#set_property IOSTANDARD LVCMOS18  [get_ports gpio[2]]
#set_property DRIVE 4              [get_ports gpio[2]]
#set_property SLEW SLOW            [get_ports gpio[2]]
##set_property PACKAGE_PIN P1      [get_ports TP9]
#set_property PACKAGE_PIN E8       [get_ports gpio[2]]
#set_property IOSTANDARD LVCMOS18  [get_ports gpio[3]]
#set_property DRIVE 4              [get_ports gpio[3]]
#set_property SLEW SLOW            [get_ports gpio[3]]
#set_property PACKAGE_PIN F4       [get_ports gpio[3]]

set_property IOSTANDARD LVCMOS18  [get_ports rx_frame]
set_property PACKAGE_PIN J3       [get_ports rx_frame]
set_property IOSTANDARD LVCMOS18  [get_ports data_clk]
set_property PACKAGE_PIN K4       [get_ports data_clk]
#set_property IOSTANDARD LVCMOS18  [get_ports rfic_en_agc]
#set_property DRIVE 4              [get_ports rfic_en_agc]
#set_property SLEW SLOW            [get_ports rfic_en_agc]
#set_property PACKAGE_PIN H5       [get_ports rfic_en_agc]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_frame]
set_property PACKAGE_PIN E4       [get_ports tx_frame]
set_property IOSTANDARD  LVCMOS18 [get_ports fb_clk]
set_property PACKAGE_PIN D3       [get_ports fb_clk]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_0]
set_property PACKAGE_PIN C2       [get_ports tx_data_0]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_1]
set_property PACKAGE_PIN D2       [get_ports tx_data_1]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_2]
set_property PACKAGE_PIN D1       [get_ports tx_data_2]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_3]
set_property PACKAGE_PIN E1       [get_ports tx_data_3]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_4]
set_property PACKAGE_PIN F1       [get_ports tx_data_4]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_5]
set_property PACKAGE_PIN G1       [get_ports tx_data_5]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_6]
set_property PACKAGE_PIN G2       [get_ports tx_data_6]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_7]
set_property PACKAGE_PIN H2       [get_ports tx_data_7]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_8]
set_property PACKAGE_PIN F2       [get_ports tx_data_8]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_9]
set_property PACKAGE_PIN F3       [get_ports tx_data_9]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_10]
set_property PACKAGE_PIN G4       [get_ports tx_data_10]
set_property IOSTANDARD  LVCMOS18 [get_ports tx_data_11]
set_property PACKAGE_PIN H4       [get_ports tx_data_11]

#set_property IOSTANDARD  LVCMOS18 [get_ports rfic_ctrl_in[0]]
#set_property DRIVE       4        [get_ports rfic_ctrl_in[0]]
#set_property SLEW        SLOW     [get_ports rfic_ctrl_in[0]]
#set_property PACKAGE_PIN A2       [get_ports rfic_ctrl_in[0]]
#set_property IOSTANDARD  LVCMOS18 [get_ports rfic_ctrl_in[1]]
#set_property DRIVE       4        [get_ports rfic_ctrl_in[1]]
#set_property SLEW        SLOW     [get_ports rfic_ctrl_in[1]]
#set_property PACKAGE_PIN A3       [get_ports rfic_ctrl_in[1]]
#set_property IOSTANDARD  LVCMOS18 [get_ports rfic_ctrl_in[2]]
#set_property DRIVE       4        [get_ports rfic_ctrl_in[2]]
#set_property SLEW        SLOW     [get_ports rfic_ctrl_in[2]]
#set_property PACKAGE_PIN B4       [get_ports rfic_ctrl_in[2]]
#set_property IOSTANDARD  LVCMOS18 [get_ports rfic_ctrl_in[3]]
#set_property DRIVE       4        [get_ports rfic_ctrl_in[3]]
#set_property SLEW        SLOW     [get_ports rfic_ctrl_in[3]]
#set_property PACKAGE_PIN A4       [get_ports rfic_ctrl_in[3]]
#set_property IOSTANDARD LVCMOS18  [get_ports ref_40mhz]
#set_property PACKAGE_PIN B1       [get_ports ref_40mhz]
#set_property IOSTANDARD  LVCMOS18 [get_ports adc_dig_bus_a[0]]
#set_property PACKAGE_PIN E5       [get_ports adc_dig_bus_a[0]]
#set_property IOSTANDARD  LVCMOS18 [get_ports adc_dig_bus_a[1]]
#set_property PACKAGE_PIN B6       [get_ports adc_dig_bus_a[1]]
#set_property IOSTANDARD  LVCMOS18 [get_ports adc_dig_bus_a[2]]
#set_property PACKAGE_PIN D5       [get_ports adc_dig_bus_a[2]]
#set_property IOSTANDARD  LVCMOS18 [get_ports adc_dig_bus_a[3]]
#set_property PACKAGE_PIN C5       [get_ports adc_dig_bus_a[3]]
#set_property IOSTANDARD  LVCMOS18 [get_ports adc_dig_bus_a[4]]
#set_property PACKAGE_PIN B5       [get_ports adc_dig_bus_a[4]]
#set_property IOSTANDARD  LVCMOS18 [get_ports adc_dig_bus_a[5]]
#set_property PACKAGE_PIN A6       [get_ports adc_dig_bus_a[5]]
#set_property IOSTANDARD  LVCMOS18 [get_ports adc_dig_bus_a[6]]
#set_property PACKAGE_PIN B7       [get_ports adc_dig_bus_a[6]]
#set_property IOSTANDARD  LVCMOS18 [get_ports adc_dig_bus_a[7]]
#set_property PACKAGE_PIN A7       [get_ports adc_dig_bus_a[7]]
set_property IOSTANDARD  LVCMOS18 [get_ports spi_miso]
set_property PACKAGE_PIN D8       [get_ports spi_miso]
set_property IOSTANDARD  LVCMOS18 [get_ports spi_mosi]
set_property DRIVE       4        [get_ports spi_mosi]
set_property SLEW        SLOW     [get_ports spi_mosi]
set_property PACKAGE_PIN D7       [get_ports spi_mosi]
set_property IOSTANDARD LVCMOS18  [get_ports ref_int_mems]
set_property DRIVE 4              [get_ports ref_int_mems]
set_property SLEW SLOW            [get_ports ref_int_mems]
set_property PACKAGE_PIN P5       [get_ports ref_int_mems]
set_property IOSTANDARD LVCMOS18  [get_ports ref_ext_10m]
set_property DRIVE 4              [get_ports ref_ext_10m]
set_property SLEW SLOW            [get_ports ref_ext_10m]
set_property PACKAGE_PIN R4       [get_ports ref_ext_10m]
set_property IOSTANDARD LVCMOS18  [get_ports rfic_enable]
set_property DRIVE 4              [get_ports rfic_enable]
set_property SLEW SLOW            [get_ports rfic_enable]
set_property PACKAGE_PIN F6       [get_ports rfic_enable]
set_property IOSTANDARD LVCMOS18  [get_ports rfic_txnrx]
set_property DRIVE 4              [get_ports rfic_txnrx]
set_property SLEW SLOW            [get_ports rfic_txnrx]
set_property PACKAGE_PIN G7       [get_ports rfic_txnrx]
#set_property IOSTANDARD LVCMOS18  [get_ports pl_ctrl_tdd_txrx_n]
#set_property DRIVE 4              [get_ports pl_ctrl_tdd_txrx_n]
#set_property SLEW SLOW            [get_ports pl_ctrl_tdd_txrx_n]
#set_property PACKAGE_PIN E6       [get_ports pl_ctrl_tdd_txrx_n]
set_property IOSTANDARD  LVCMOS18 [get_ports rf_rst_n]
set_property DRIVE       4        [get_ports rf_rst_n]
set_property SLEW        SLOW     [get_ports rf_rst_n]
set_property PACKAGE_PIN F8       [get_ports rf_rst_n]
#set_property IOSTANDARD  LVCMOS18 [get_ports multi_sync]
#set_property DRIVE       4        [get_ports multi_sync]
#set_property SLEW        SLOW     [get_ports multi_sync]
#set_property PACKAGE_PIN F7       [get_ports multi_sync]
set_property IOSTANDARD LVCMOS18  [get_ports ref_ext_40m]
set_property DRIVE 4              [get_ports ref_ext_40m]
set_property SLEW SLOW            [get_ports ref_ext_40m]
set_property PACKAGE_PIN R5       [get_ports ref_ext_40m]
set_property IOSTANDARD  LVCMOS18 [get_ports spi_en_lo]
set_property DRIVE       4        [get_ports spi_en_lo]
set_property SLEW        SLOW     [get_ports spi_en_lo]
set_property PACKAGE_PIN C8       [get_ports spi_en_lo]
set_property IOSTANDARD  LVCMOS18 [get_ports spi_clk]
set_property DRIVE       4        [get_ports spi_clk]
set_property SLEW        SLOW     [get_ports spi_clk]
set_property PACKAGE_PIN C7       [get_ports spi_clk]
set_property IOSTANDARD  LVCMOS18 [get_ports sda_1]
set_property DRIVE       4        [get_ports sda_1]
set_property SLEW        SLOW     [get_ports sda_1]
set_property PACKAGE_PIN G6       [get_ports sda_1]
set_property IOSTANDARD  LVCMOS18 [get_ports scl_1]
set_property DRIVE       4        [get_ports scl_1]
set_property SLEW        SLOW     [get_ports scl_1]
set_property PACKAGE_PIN G5       [get_ports scl_1]
set_property IOSTANDARD  LVCMOS18 [get_ports scl]
set_property DRIVE       4        [get_ports scl]
set_property SLEW        SLOW     [get_ports scl]
set_property PACKAGE_PIN B9       [get_ports scl]
set_property IOSTANDARD  LVCMOS18 [get_ports sda]
set_property DRIVE       4        [get_ports sda]
set_property SLEW        SLOW     [get_ports sda]
set_property PACKAGE_PIN A9       [get_ports sda]

#######################################################################
# OpenCPI additions to the above, which is unmodified from the original
#######################################################################

#######################################################################
# Clocks
#######################################################################
create_clock -name clk_fpga_0 -period 10.000 [get_pins -hier * -filter {NAME =~ */ps/U0/inst/PS8_i/PLCLK[0]}]
set_property DONT_TOUCH true [get_cells "ftop/pfconfig_i/z3u_i/worker/ps/U0/inst/PS8_i"]

# TCXO clock 40 MHz
create_clock -period 25.000 -name ref_40mhz [get_nets ref_40mhz]
set_input_jitter ref_40mhz 0.100
#set_property CLOCK_DEDICATED_ROUTE false [get_nets ref_40mhz]

#################################################
# Additional constraints in support of the AD9361
#################################################

# AD9361 SPI clock (So slow that it may not be necessary)
create_clock -period 80.00 -name spi_clk [get_ports {spi_clk}]

# max supported AD9361 DATA_CLK period of data_src_qadc_ad9361_sub.hdl on Z3u for which slack will be 0
#create_clock -period 32 -name data_clk [get_ports {data_clk}]
# FOR NOW, THIS IS OVERKILL (this "period" is used in calculations below)
create_clock -period 16.276 -name data_clk [get_ports {data_clk}]

# fb_clk (forwarded version data_clk)
create_generated_clock -name fb_clk -source [get_pins {ftop/platform_ad9361_data_sub_i/worker/data_clk_buf_i/O}] -divide_by 1 -invert [get_ports {fb_clk}]

# Generate DAC one sample per clock constraints
# both of these create_generated_clock constraints, as well as their corresponding circuits, follow
# almost exactly the design methodology outlined in "Figure 3-70: Generated Clock in the Fanout Of
# Master Clock" in
# https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_1/ug949-vivado-design-methodology.pdf
create_generated_clock -name dac_clk_div4 -divide_by 2 -source [get_pins {ftop/data_sink_qdac_ad9361_sub_i/worker/data_mode_cmos.single_port_fdd_ddr.wsi_clk_gen/clock_manager/second_divider/divider_type_register.routability_global.divisor_2.reg_out_reg/C}] [get_pins {ftop/data_sink_qdac_ad9361_sub_i/worker/data_mode_cmos.single_port_fdd_ddr.wsi_clk_gen/clock_manager/second_divider/divider_type_register.routability_global.divisor_2.reg_out_reg/Q}]

# following methodology in "Figure 3-77: Muxed Clocks" / "Case in which only the paths A or B or C exist" in https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_1/ug949-vivado-design-methodology.pdf
create_generated_clock -name dac_clk_div2_mux -divide_by 1 -source [get_pins {ftop/data_sink_qdac_ad9361_sub_i/worker/data_mode_cmos.single_port_fdd_ddr.wsi_clk_gen/clock_manager/first_divider/divider_type_buffer.routability_regional.buffer_and_divider/O}] [get_pins {ftop/data_sink_qdac_ad9361_sub_i/worker/data_mode_cmos.single_port_fdd_ddr.wsi_clk_gen/clock_manager/clock_selector/bufgmux/O}]
create_generated_clock -name dac_clk_div4_mux -divide_by 1 -add -master_clock dac_clk_div4 -source [get_pins {ftop/data_sink_qdac_ad9361_sub_i/worker/data_mode_cmos.single_port_fdd_ddr.wsi_clk_gen/clock_manager/second_divider/divider_type_register.routability_global.divisor_2.reg_out_reg/Q}] [get_pins {ftop/data_sink_qdac_ad9361_sub_i/worker/data_mode_cmos.single_port_fdd_ddr.wsi_clk_gen/clock_manager/clock_selector/bufgmux/O}]
set_clock_groups -physically_exclusive -group dac_clk_div2_mux -group dac_clk_div4_mux

# Generate ADC one sample per clock constraints
# both of these create_generated_clock constraints, as well as their corresponding circuits, follow
# almost exactly the design methodology outlined in "Figure 3-70: Generated Clock in the Fanout Of
# Master Clock" in
# https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_1/ug949-vivado-design-methodology.pdf
create_generated_clock -name adc_clk_div2 -divide_by 2 -source [get_pins {ftop/data_src_qadc_ad9361_sub_i/worker/sample_clk_gen/clock_manager/first_divider/divider_type_register.routability_global.divisor_2.reg_out_reg/C}] [get_pins {ftop/data_src_qadc_ad9361_sub_i/worker/sample_clk_gen/clock_manager/first_divider/divider_type_register.routability_global.divisor_2.reg_out_reg/Q}]
create_generated_clock -name adc_clk_div4 -divide_by 2 -source [get_pins {ftop/data_src_qadc_ad9361_sub_i/worker/sample_clk_gen/clock_manager/second_divider/divider_type_register.routability_global.divisor_2.reg_out_reg/C}] [get_pins {ftop/data_src_qadc_ad9361_sub_i/worker/sample_clk_gen/clock_manager/second_divider/divider_type_register.routability_global.divisor_2.reg_out_reg/Q}]

# following methodology in "Figure 3-77: Muxed Clocks" / "Case in which only the paths A or B or C exist" in https://www.xilinx.com/support/documentation/sw_manuals/xilinx2017_1/ug949-vivado-design-methodology.pdf
create_generated_clock -name adc_clk_div2_mux -divide_by 1 -source [get_pins {ftop/data_src_qadc_ad9361_sub_i/worker/sample_clk_gen/clock_manager/first_divider/divider_type_register.routability_global.divisor_2.reg_out_reg/Q}] [get_pins {ftop/data_src_qadc_ad9361_sub_i/worker/sample_clk_gen/clock_manager/clock_selector/bufgmux/O}]
create_generated_clock -name adc_clk_div4_mux -divide_by 1 -add -master_clock adc_clk_div4 -source [get_pins {ftop/data_src_qadc_ad9361_sub_i/worker/sample_clk_gen/clock_manager/second_divider/divider_type_register.routability_global.divisor_2.reg_out_reg/Q}] [get_pins {ftop/data_src_qadc_ad9361_sub_i/worker/sample_clk_gen/clock_manager/clock_selector/bufgmux/O}]
set_clock_groups -physically_exclusive -group adc_clk_div2_mux -group adc_clk_div4_mux

#
# ----- assumed platform_ad9361_data_sub.hdl parameter property/no-OS init_param settings
# ----- (values chosen specifically to meet static timing):
# ------------------------------------------------------------------------------
# |                     | no-OS init_param member | value | delay(ns)          |
# | parameter property  |                         |       |                    |
# ------------------------------------------------------------------------------
# | DATA_CLK_Delay      | rx_data_clock_delay     | 7     | 2.1                |
# | Rx_Data_Delay       | rx_data_delay           | 0     | 0.0                |
# ------------------------------------------------------------------------------
#
# ----- calculations
# skew_bre = -t_DDRx_min + (DATA_CLK_Delay-Rx_Data_Delay)*0.3 = -2.1
# Rise Min = (period/2) - skew_bfe = (16.276/2) - 1.2 = 6.938
# Rise Min = (period/2) - skew_bfe = (32/2) - 1.2 = 13.33
# t_DDRx_min = 0
set min_rx_delay 6.938
#set min_rx_delay 13.33
# t_DDRx_max = 1.5 (Table 49 of UG-570 for 1.8V supply - Rx Data Delay from DATA_CLK to data pins)
set max_rx_delay [expr $min_rx_delay + 1.5]
# t_DDRv_min = 0
set min_rx_frame_delay $min_rx_delay
# t_DDRv_max = 1.0 (Rx Data Delay from DATA_CLK to RX_FRAME)
set max_rx_frame_delay [expr $min_rx_delay + 1]

set_input_delay -clock [get_clocks data_clk] -max $max_rx_frame_delay [get_ports {rx_frame}]
set_input_delay -clock [get_clocks data_clk] -min $min_rx_frame_delay [get_ports {rx_frame}]
set_input_delay -clock [get_clocks data_clk] -max $max_rx_frame_delay [get_ports {rx_frame}] -clock_fall -add_delay
set_input_delay -clock [get_clocks data_clk] -min $min_rx_frame_delay [get_ports {rx_frame}] -clock_fall -add_delay

# because RX_FRAME_P is sampled on the DATA_CLK_P falling edge (we use DDR primitive as a sample-in-the-middle), the rising edge latched output is unconnected and therefore should not be used in timing analysis
set_false_path -from [get_ports rx_frame] -rise_to [get_pins {ftop/data_src_qadc_ad9361_sub_i/worker/supported_so_far.rx_frame_p_ddr/D}]

#
# ----- assumed platform_ad9361_data_sub.hdl parameter property/no-OS init_param settings
# ----- (values chosen specifically to meet static timing):
# ------------------------------------------------------------------------------
# |                     | no-OS init_param member | value | delay(ns)          |
# | parameter property  |                         |       |                    |
# ------------------------------------------------------------------------------
# | FB_CLK_Delay        | tx_fb_clock_delay       | 12    | 3.6                |
# | TX_Data_Delay       | tx_data_delay           | 0     | 0.0                |
# ------------------------------------------------------------------------------
#
# From observation, I am guessing that cat_fb_data_prog_dly is equivalent to
#   (FB_CLK_Delay-TX_Data_Delay)*0.3 = 3.6
#     ==> (FB_CLK_Delay-TX_Data_Delay) = 12
# If we set TX_Data_Delay to 0, this leads to an FB_CLK_Delay of 12
set fb_data_prog_dly            3.6;  # Programmable skew set to delay TX data by 3.6 ns
# t_STx_max=1 (Table 49 of UG-570)
set fb_data_setup               1.0;
# t_HTx_max=0 (Table 49 of UG-570)
set fb_data_hold                0;

set min_tx_delay [expr $fb_data_prog_dly - $fb_data_hold ]
set max_tx_delay [expr $fb_data_prog_dly - $fb_data_setup ]

set_output_delay -clock fb_clk -max $max_tx_delay [get_ports {tx_frame}] -add_delay
set_output_delay -clock fb_clk -min $min_tx_delay [get_ports {tx_frame}] -add_delay
set_output_delay -clock fb_clk -max $max_tx_delay [get_ports {tx_frame}] -clock_fall -add_delay
set_output_delay -clock fb_clk -min $min_tx_delay [get_ports {tx_frame}] -clock_fall -add_delay

###############################################################################
## Asynchronous paths (CLOCK DOMAIN CROSSING / FALSE PATH constraints)
###############################################################################

set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks fb_clk]
set_false_path -from [get_clocks clk_fpga_0] -to [get_clocks data_clk]
set_false_path -from [get_clocks data_clk]   -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks fb_clk]     -to [get_clocks clk_fpga_0]

set_false_path -from [get_clocks dac_clk_div4]     -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks clk_fpga_0]       -to [get_clocks dac_clk_div4]
set_false_path -from [get_clocks dac_clk_div4    ] -to [get_clocks data_clk]
set_false_path -from [get_clocks data_clk]         -to [get_clocks dac_clk_div4]

set_false_path -from [get_clocks dac_clk_div2_mux] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks clk_fpga_0]       -to [get_clocks dac_clk_div2_mux]
set_false_path -from [get_clocks dac_clk_div2_mux] -to [get_clocks data_clk]
set_false_path -from [get_clocks data_clk]         -to [get_clocks dac_clk_div2_mux]

set_false_path -from [get_clocks dac_clk_div4_mux] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks clk_fpga_0]       -to [get_clocks dac_clk_div4_mux]
set_false_path -from [get_clocks dac_clk_div4_mux] -to [get_clocks data_clk]
set_false_path -from [get_clocks data_clk]         -to [get_clocks dac_clk_div4_mux]

set_false_path -from [get_clocks adc_clk_div2] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks clk_fpga_0]       -to [get_clocks adc_clk_div2]
set_false_path -from [get_clocks adc_clk_div2] -to [get_clocks data_clk]
set_false_path -from [get_clocks data_clk]         -to [get_clocks adc_clk_div2]

set_false_path -from [get_clocks adc_clk_div4] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks clk_fpga_0]       -to [get_clocks adc_clk_div4]
set_false_path -from [get_clocks adc_clk_div4] -to [get_clocks data_clk]
set_false_path -from [get_clocks data_clk]         -to [get_clocks adc_clk_div4]

set_false_path -from [get_clocks adc_clk_div2_mux] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks clk_fpga_0]       -to [get_clocks adc_clk_div2_mux]
set_false_path -from [get_clocks adc_clk_div2_mux] -to [get_clocks data_clk]
set_false_path -from [get_clocks data_clk]         -to [get_clocks adc_clk_div2_mux]

set_false_path -from [get_clocks adc_clk_div4_mux] -to [get_clocks clk_fpga_0]
set_false_path -from [get_clocks clk_fpga_0]       -to [get_clocks adc_clk_div4_mux]
set_false_path -from [get_clocks adc_clk_div4_mux] -to [get_clocks data_clk]
set_false_path -from [get_clocks data_clk]         -to [get_clocks adc_clk_div4_mux]

##############################################################################
## I/O Path "Data" Delay
###############################################################################
set_input_delay -clock [get_clocks {data_clk}] -clock_fall -min -add_delay $min_rx_delay [get_ports {tx_data_0}]
set_input_delay -clock [get_clocks {data_clk}] -clock_fall -max -add_delay $max_rx_delay [get_ports {tx_data_0}]
set_input_delay -clock [get_clocks {data_clk}]             -min -add_delay $min_rx_delay [get_ports {tx_data_0}]
set_input_delay -clock [get_clocks {data_clk}]             -max -add_delay $max_rx_delay [get_ports {tx_data_0}]

set_input_delay -clock [get_clocks {data_clk}] -clock_fall -min -add_delay $min_rx_delay [get_ports {tx_data_1}]
set_input_delay -clock [get_clocks {data_clk}] -clock_fall -max -add_delay $max_rx_delay [get_ports {tx_data_1}]
set_input_delay -clock [get_clocks {data_clk}]             -min -add_delay $min_rx_delay [get_ports {tx_data_1}]
set_input_delay -clock [get_clocks {data_clk}]             -max -add_delay $max_rx_delay [get_ports {tx_data_1}]

set_input_delay -clock [get_clocks {data_clk}] -clock_fall -min -add_delay $min_rx_delay [get_ports {tx_data_2}]
set_input_delay -clock [get_clocks {data_clk}] -clock_fall -max -add_delay $max_rx_delay [get_ports {tx_data_2}]
set_input_delay -clock [get_clocks {data_clk}]             -min -add_delay $min_rx_delay [get_ports {tx_data_2}]
set_input_delay -clock [get_clocks {data_clk}]             -max -add_delay $max_rx_delay [get_ports {tx_data_2}]

set_input_delay -clock [get_clocks {data_clk}] -clock_fall -min -add_delay $min_rx_delay [get_ports {tx_data_3}]
set_input_delay -clock [get_clocks {data_clk}] -clock_fall -max -add_delay $max_rx_delay [get_ports {tx_data_3}]
set_input_delay -clock [get_clocks {data_clk}]             -min -add_delay $min_rx_delay [get_ports {tx_data_3}]
set_input_delay -clock [get_clocks {data_clk}]             -max -add_delay $max_rx_delay [get_ports {tx_data_3}]

set_input_delay -clock [get_clocks {data_clk}] -clock_fall -min -add_delay $min_rx_delay [get_ports {tx_data_4}]
set_input_delay -clock [get_clocks {data_clk}] -clock_fall -max -add_delay $max_rx_delay [get_ports {tx_data_4}]
set_input_delay -clock [get_clocks {data_clk}]             -min -add_delay $min_rx_delay [get_ports {tx_data_4}]
set_input_delay -clock [get_clocks {data_clk}]             -max -add_delay $max_rx_delay [get_ports {tx_data_4}]

set_input_delay -clock [get_clocks {data_clk}] -clock_fall -min -add_delay $min_rx_delay [get_ports {tx_data_5}]
set_input_delay -clock [get_clocks {data_clk}] -clock_fall -max -add_delay $max_rx_delay [get_ports {tx_data_5}]
set_input_delay -clock [get_clocks {data_clk}]             -min -add_delay $min_rx_delay [get_ports {tx_data_5}]
set_input_delay -clock [get_clocks {data_clk}]             -max -add_delay $max_rx_delay [get_ports {tx_data_5}]

set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -min -add_delay $min_tx_delay [get_ports {tx_data_6}]
set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -max -add_delay $max_tx_delay [get_ports {tx_data_6}]
set_output_delay -clock [get_clocks {fb_clk}]             -min -add_delay $min_tx_delay [get_ports {tx_data_6}]
set_output_delay -clock [get_clocks {fb_clk}]             -max -add_delay $max_tx_delay [get_ports {tx_data_6}]

set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -min -add_delay $min_tx_delay [get_ports {tx_data_7}]
set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -max -add_delay $max_tx_delay [get_ports {tx_data_7}]
set_output_delay -clock [get_clocks {fb_clk}]             -min -add_delay $min_tx_delay [get_ports {tx_data_7}]
set_output_delay -clock [get_clocks {fb_clk}]             -max -add_delay $max_tx_delay [get_ports {tx_data_7}]

set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -min -add_delay $min_tx_delay [get_ports {tx_data_8}]
set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -max -add_delay $max_tx_delay [get_ports {tx_data_8}]
set_output_delay -clock [get_clocks {fb_clk}]             -min -add_delay $min_tx_delay [get_ports {tx_data_8}]
set_output_delay -clock [get_clocks {fb_clk}]             -max -add_delay $max_tx_delay [get_ports {tx_data_8}]

set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -min -add_delay $min_tx_delay [get_ports {tx_data_9}]
set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -max -add_delay $max_tx_delay [get_ports {tx_data_9}]
set_output_delay -clock [get_clocks {fb_clk}]             -min -add_delay $min_tx_delay [get_ports {tx_data_9}]
set_output_delay -clock [get_clocks {fb_clk}]             -max -add_delay $max_tx_delay [get_ports {tx_data_9}]

set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -min -add_delay $min_tx_delay [get_ports {tx_data_10}]
set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -max -add_delay $max_tx_delay [get_ports {tx_data_10}]
set_output_delay -clock [get_clocks {fb_clk}]             -min -add_delay $min_tx_delay [get_ports {tx_data_10}]
set_output_delay -clock [get_clocks {fb_clk}]             -max -add_delay $max_tx_delay [get_ports {tx_data_10}]

set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -min -add_delay $min_tx_delay [get_ports {tx_data_11}]
set_output_delay -clock [get_clocks {fb_clk}] -clock_fall -max -add_delay $max_tx_delay [get_ports {tx_data_11}]
set_output_delay -clock [get_clocks {fb_clk}]             -min -add_delay $min_tx_delay [get_ports {tx_data_11}]
set_output_delay -clock [get_clocks {fb_clk}]             -max -add_delay $max_tx_delay [get_ports {tx_data_11}]
